#include <stdio.h>
#include <math.h>

#define N 21

double sumArray(double *array, int size);
void bubbleSort(double *array, int size);

int main(int argc, char *argv[]) {
  double data_set[] = {
    1.0e16, -1.0e2, 23, -6.4, 3.6e2, -0.01, 8.0, -70, 5.0e3, 1.2e-2, -3.0e3,
    46, -1.7e3, 10, -5.0e2, 7.0, -2.0e-3, 0.3, -30, 3.1, -1.0e16
  };

  for (int i = 0; i < N; i++) {
    printf("%g ", data_set[i]);
  }
  printf("\n");
  printf("before sort : %lf\n", sumArray(data_set, N));

  bubbleSort(data_set, N);

  for (int i = 0; i < N; i++) {
    printf("%g ", data_set[i]);
  }
  printf("\n");
  printf("after sort : %lf\n", sumArray(data_set, N));

  return 0;
}

double sumArray(double *array, int size) {
  double sum = 0;
  for (int i = 0; i < size; i++) {
    sum += array[i];
  }

  return sum;
}

void bubbleSort(double *array, int size) {
  for (int i = 0; i < size; i++) {
    for (int j = size - 1; j > i; j--) {
      if (fabs(array[j - 1]) > fabs(array[j])) {
        double tmp = array[j];
        array[j] = array[j - 1];
        array[j - 1] = tmp;
      }
    }
  }
}
