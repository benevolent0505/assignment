#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 10

typedef struct {
  char* firstname;
  char* lastname;
} Name;

typedef struct __nameNode {
  Name name;
  struct __nameNode *next;
} nameNode;

void initialize(nameNode **p);
nameNode *newNode(Name name, nameNode *p);
void insertNode(nameNode **p, Name name, int index);
int getIndexByLastName(nameNode *p, char* lastname);
int getIndexByFirstName(nameNode *p, char* firstname);
void printNodes(nameNode *p);

int main(int argc, char *argv[]) {
  Name member[] = {
    {"Leroy", "Richardson"},
    {"Casey",	"Grant"},
    {"Dawn",	"Hall"},
    {"Scott",	"Lyons"},
    {"Mona",	"Singleton"},
    {"Guadalupe", "Kelly"},
    {"Wendy",	"Oliver"},
    {"Julio",	"Frank"},
    {"Martin",	"Gibbs"},
    {"Ervin",	"Fletcher"}
  };

  nameNode *last;
  initialize(&last);

  for (int i = 0; i < N; i++) {
    int pos = getIndexByLastName(last, member[i].lastname);
    insertNode(&last, member[i], pos);
  }
  printNodes(last);

  puts("");
  puts("sort by first name");
  puts("");

  nameNode *first;
  initialize(&first);

  while (last != NULL) {
    int pos = getIndexByFirstName(first, last -> name.firstname);
    insertNode(&first, last -> name, pos);

    last = last -> next;
  }

  printNodes(first);

  return 0;
}

void initialize(nameNode **p) {
  *p = NULL;
}

void printNodes(nameNode *p) {
  if (p != NULL) {
    printf("%s ", p -> name.firstname);
    printf("%s\n", p -> name.lastname);
    printNodes(p -> next);
  }
}

nameNode *newNode(Name name, nameNode *p) {
  nameNode *tmp;

  tmp = (nameNode *)malloc(sizeof(nameNode));

  if (tmp != NULL) {
    tmp -> name = name;
    tmp -> next = p;

    return tmp;
  } else {
    return NULL;
  }
}

void insertNode(nameNode **p, Name name, int index) {
  for (int i = 0; i < index && *p != NULL; i++) {
    p = &((*p) -> next);
  }

  nameNode *tmp = newNode(name, *p);

  *p = tmp;
}

int getIndexByLastName(nameNode *p, char* lastname) {
  int i;
  for (i = 0; (p != NULL) && strcmp(lastname, p -> name.lastname) >= 0; i++) {
    p = p -> next;
  }

  return i;
}

int getIndexByFirstName(nameNode *p, char* firstname) {
  int i;
  for (i = 0; (p != NULL) && strcmp(firstname, p -> name.firstname) >= 0; i++) {
    p = p -> next;
  }

  return i;
}
