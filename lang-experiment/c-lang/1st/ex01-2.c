#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 21

typedef double data_type;
typedef struct node_tag {
  data_type data;
  struct node_tag *next;
} node_type;

void initialize(node_type **p);
void printNodes(node_type *p);
node_type *newNode(data_type x, node_type *p);
void insertNode(node_type **p, data_type x, int index);
int searchPosition(node_type *p, data_type x);
double sumNodes(node_type *p);

int main(int argc, char *argv[]) {
  double data_set[] = {
    1.0e16, -1.0e2, 23, -6.4, 3.6e2, -0.01, 8.0, -70, 5.0e3, 1.2e-2, -3.0e3,
     46, -1.7e3, 10, -5.0e2, 7.0, -2.0e-3, 0.3, -30, 3.1, -1.0e16
  };

  node_type *head;
  initialize(&head);

  for (int i = 0; i < N; i++) {
    insertNode(&head, data_set[i], i);
  }
  printNodes(head);
  printf("before sort: %lf\n", sumNodes(head));

  node_type *sort;
  initialize(&sort);

  for (int i = 0; i < N; i++) {
    int pos = searchPosition(sort, data_set[i]);
    insertNode(&sort, data_set[i], pos);
  }
  printNodes(sort);
  printf("after sort: %lf\n", sumNodes(sort));

  return 0;
}

void initialize(node_type **p) {
  *p = NULL;
}

void printNodes(node_type *p) {
  if (p != NULL) {
    printf("%g ", p -> data);
    printNodes(p -> next);
  } else {
    printf("\n");
  }
}

node_type *newNode(data_type x, node_type *p) {
  node_type *tmp;

  tmp = (node_type *)malloc(sizeof(node_type));

  if (tmp != NULL) {
    tmp -> data = x;
    tmp -> next = p;

    return tmp;
  } else {
    return NULL;
  }
}

void insertNode(node_type **p, data_type x, int index) {
  for (int i = 0; i < index && *p != NULL; i++) {
    p = &((*p) -> next);
  }

  node_type *tmp = newNode(x, *p);

  *p = tmp;
}

int searchPosition(node_type *p, data_type x) {
  int i;
  for (i = 0; (p != NULL) && fabs(x) > fabs(p -> data); i++) {
    p = p -> next;
  }

  return i;
}

double sumNodes(node_type *p) {
  double sum = 0;

  while (p != NULL) {
    sum += p -> data;
    p = p -> next;
  }

  return sum;
}
