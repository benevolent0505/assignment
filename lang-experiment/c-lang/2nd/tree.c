#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0

#define SUCCESS 1
#define FAILURE 0

#define N 21

typedef double data_type;
typedef struct node_tag {
  data_type data;
  struct node_tag *left;
  struct node_tag *right;
} node_type;

void initialize(node_type **pp);
int isMember(data_type x, node_type *p);
node_type *newNode(data_type x);
int insert(node_type **pp, data_type x);

void inorder(node_type *p);
double sumTree(node_type *p);

int main(int argc, char *argv[]) {
  double data_set[] = {
    1.0e16, -1.0e2, 23, -6.4, 3.6e2, -0.01, 8.0, -70, 5.0e3, 1.2e-2, -3.0e3,
    46, -1.7e3, 10, -5.0e2, 7.0, -2.0e-3, 0.3, -30, 3.1, -1.0e16
  };

  node_type *root;
  initialize(&root);

  for (int i = 0; i < N; i++) {
    insert(&root, data_set[i]);
  }

  inorder(root);
  printf("\n");
  printf("sum is %lf\n", sumTree(root));

  return 0;
}

void initialize(node_type **pp) {
  *pp = NULL;
}

int isMember(data_type x, node_type *p) {
  if (p == NULL) {
    return FALSE;
  } else {
    if (x == p -> data) {
      return TRUE;
    } else {
      if (x < p -> data) {
        return isMember(x, p -> left);
      } else {
        return isMember(x, p -> right);
      }
    }
  }
}

node_type *newNode(data_type x) {
  node_type *tmp = (node_type *)malloc(sizeof(node_type));

  if (tmp != NULL) {
    tmp -> data = x;
    tmp -> left = NULL;
    tmp -> right = NULL;

    return tmp;
  } else {
    return NULL;
  }
}

/* 絶対値での評価込み */
int insert(node_type **pp, data_type x) {
  if (*pp == NULL) {
    node_type *tmp = newNode(x);

    if (tmp == NULL)
      return FAILURE;

    *pp = tmp;
    return SUCCESS;
  } else {
    if (fabs(x) < fabs((*pp) -> data)) {
      return insert(&((*pp) -> left), x);
    } else if (fabs(x) >= fabs((*pp) -> data)) {
      return insert(&((*pp) -> right), x);
    } else {
      return FAILURE;
    }
  }
}

void inorder(node_type *p) {
  if (p != NULL) {
    inorder(p -> left);
    printf("%g ", p -> data);
    inorder(p -> right);
  }
}

double sumTree(node_type *p) {
  if (p != NULL) {
    return sumTree(p -> left) + p -> data + sumTree(p -> right);
  } else {
    return 0;
  }
}
