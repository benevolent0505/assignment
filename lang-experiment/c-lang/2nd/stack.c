#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

#define SUCCESS 1
#define FAILURE 0

typedef char data_type;
typedef struct node_tag {
  data_type data;
  struct node_tag *next;
} node_type;

void reversePolish(char *exp);
int getPriorityNum(char token);

void initialize(node_type **pp);
node_type *newNode(data_type x, node_type *p);
int isEmpty(node_type *p);
data_type getTop(node_type *p);
int push(node_type **pp, data_type x);
int pop(node_type **pp);

int main(int argc, char *argv[]) {
  char *exp1 = "A = (B - C) / D + E * F";
  char *exp2 = "A = B - (C / D + E) * F";
  char *exp3 = "A = B - C / (D + E * F)";

  reversePolish(exp1);
  puts("\n");
  reversePolish(exp2);
  puts("\n");
  reversePolish(exp3);

  return 0;
}

/* 変換結果を出力する */
void reversePolish(char *exp) {
  node_type *stack;
  initialize(&stack);

  while (*exp != '\0') {
    while (isEmpty(stack) == FAILURE
           && getTop(stack) != '('
           && getPriorityNum(*exp) <= getPriorityNum(getTop(stack))) {
      printf("%c", stack -> data);
      pop(&stack);
    }

    if (*exp != ')') {
      push(&stack, *exp);
    } else {
      pop(&stack);
    }
    exp++;
  }

  while (isEmpty(stack) == FAILURE) {
    printf("%c", stack -> data);
    pop(&stack);
  }
}

int getPriorityNum(char token) {
  switch (token) {
  case '=':
    return 0;
  case '(':
    return 4;
  case ')':
    return 1;
  case '+':
    return 2;
  case '-':
    return 2;
  case '*':
    return 3;
  case '/':
    return 3;
  default:
    return 5;
  }
}

void initialize(node_type **pp) {
  *pp = NULL;
}

node_type *newNode(data_type x, node_type *p) {
  node_type *tmp = (node_type *)malloc(sizeof(node_type));

  if (tmp != NULL) {
    tmp -> data = x;
    tmp -> next = p;

    return tmp;
  } else {
    return NULL;
  }
}

int isEmpty(node_type *p) {
  if (p == NULL) {
    return TRUE;
  } else {
    return FALSE;
  }
}

data_type getTop(node_type *p) {
  if (p == NULL) {
    return '\0';
  } else {
    return p -> data;
  }
}

int push(node_type **pp, data_type x) {
  node_type *tmp = newNode(x, *pp);

  if (tmp == NULL)
    return FAILURE;

  *pp = tmp;

  return SUCCESS;
}

int pop(node_type **pp) {
  node_type *tmp;

  if (*pp != NULL) {
    tmp = (*pp) -> next;

    free(*pp);

    *pp = tmp;

    return SUCCESS;
  } else {
    return FAILURE;
  }
}
