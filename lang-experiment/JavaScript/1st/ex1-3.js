const COUNT = document.querySelector('tbody').rows.length;

document.addEventListener('DOMContentLoaded', function() {
  document.querySelector('button').addEventListener('click', function() {
    let number = document.querySelector('#number').value;

    document.querySelector('#n').textContent = number;

    for (var i = 0; i < COUNT; i++) {
      let tdValue = document.querySelector('#id-' + i.toString());
      tdValue.textContent = Math.pow(parseInt(number), i);
    }
  });
});
