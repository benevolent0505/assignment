document.addEventListener('DOMContentLoaded', function() {
  var countDiv = document.getElementById('res-count');
  var sentenceDiv = document.getElementById('res-sentence');

  document.getElementById('button').addEventListener('click', function() {
    let value = document.getElementById('value').value;
    let keyword = document.getElementById('key').value;

    countDiv.textContent = countKey(value, keyword);
    sentenceDiv.textContent = generateNewSentence(value, keyword);
  });
});

function generateNewSentence(value, keyword) {
  return value.replace(new RegExp(keyword, "g"), '');
}

function countKey(value, key) {
  return value.split(key).length - 1;
}
