const URL_1 = 'https://www.ied.inf.uec.ac.jp/text/laboratory/javascript/img1.png';
const URL_2 = 'https://www.ied.inf.uec.ac.jp/text/laboratory/javascript/img2.png';
const URL_3 = 'https://www.ied.inf.uec.ac.jp/text/laboratory/javascript/img3.png';

const URLS = [URL_1, URL_2, URL_3];

var intervalID = '';

document.addEventListener('DOMContentLoaded', function() {
  let img = document.querySelector('img');
  img.src = URLS[0];

  document.querySelector('#start').addEventListener('click', function() {
    intervalID = setInterval(function () {
      changeImage(img, URLS);
    }, 5000);
  });

  document.querySelector('#stop').addEventListener('click', function() {
    if (intervalID) {
      clearInterval(intervalID);
    }
  });
});

function changeImage(img, srcArray) {
  if (img.src == srcArray[0]) {
    img.src = srcArray[1];
  } else if (img.src == srcArray[1]) {
    img.src = srcArray[2];
  } else if (img.src == srcArray[2]) {
    img.src = srcArray[0];
  }
}
