document.addEventListener('DOMContentLoaded', function() {
  document.querySelector('img').addEventListener('click', function() {
    biggerImage(document.querySelector('img'));
  });
});

function biggerImage(image) {
  image.width += 10;
  image.height += 10;
}
