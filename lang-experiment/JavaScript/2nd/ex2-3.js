'use strict';

document.addEventListener('DOMContentLoaded', function() {
  let image = document.querySelector('img');

  // imgタグのスタイルを初期化
  image.style.left = '0px';

  let intervalID = setInterval(() => {
    // 10px追加した値を代入
    image.style.left = convertPx(parseInt(image.style.left) + 10);

    // 400pxを越すと終了
    if (parseInt(image.style.left) >= 400) {
      clearInterval(intervalID);
    }
  }, 100);

  // 数値を文字列("数値px")に変換する関数
  function convertPx(value) {
    return value + 'px';
  }
});
