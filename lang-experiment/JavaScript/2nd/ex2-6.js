'use strict';

document.addEventListener('DOMContentLoaded', function() {
  if (getCookieData('count')) {
    // 2回目以降

    // not-firstのclassのタグを表示
    let elements = document.querySelectorAll('.not-first');
    Array.prototype.forEach.call(elements, function(elem) {
      elem.style.display = 'inline'; // display: noneをinlineにして表示
    });

    // クリックイベントを登録
    document.querySelector('#update').addEventListener('click', updateUserData);

    // cookieに保存されているデータを表示
    document.querySelector('#name-display').textContent = getCookieData('name');
    document.querySelector('#hobby-display').textContent = getCookieData('hobby');

    updateVisitCount();
    // 訪問回数をカウントしてから表示させる
    document.querySelector('#count').textContent = getCookieData('count');
  } else {
    // 初回訪問時
    document.querySelector('#submit').addEventListener('click', saveUserData);

    let elements = document.querySelectorAll('.first');
    Array.prototype.forEach.call(elements, (elem) => {
      elem.style.display = 'inline';
    });

    // 訪問回数をカウント
    updateVisitCount();
  }


  // 初回の保存時用
  function saveUserData() {
    let name = document.querySelector('#name').value;
    let hobby = document.querySelector('#hobby').value;
    let limit = new Date();
    limit.setDate(limit.getDate() + 1);

    setDataToCookie('name', name);
    setDataToCookie('hobby', hobby);
    setDataToCookie('expires', limit.toGMTString());
  }

  // 趣味更新時用
  function updateUserData() {
    let hobby = document.querySelector('#new-hobby').value;

    setDataToCookie('hobby', hobby);
  }

  // 訪問時に呼び出される関数
  function updateVisitCount() {
    let count = parseInt(getCookieData('count'));

    if (count) {
      // ２回目以降
      setDataToCookie('count', count + 1);
    } else {
      // 初回は
      setDataToCookie('count', 1);
    }
  }

  // cookie保存時用
  function setDataToCookie(key, value) {
    document.cookie = key + '=' + escape(value);
  }

  // 文字列しか返さないので注意!!
  function getCookieData(key) {
    // '; 'でcookieを区切る
    return document.cookie.split('; ').filter((text) => {
      // '='で区切った最初の要素と引数のkeyが一致したものを返えす
      return text.split('=')[0] == key;
    }).map((cookie) => {
      // keyは要らないのでvalueだけにして返えす
      return unescape(cookie.split('=')[1]);
    }).shift();
  }
});
