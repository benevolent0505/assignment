'use strict';

document.addEventListener('DOMContentLoaded', function() {
  // querySelectorAllで返ってくるオブジェクトはNodeListなのでArrayのforEachやsliceメソッドが使えない.
  // その為callメソッドを使ってNodeListでArrayオブジェクトのsliceメソッドを使えるようにしている.
  // callは第一引数にメソッドを実行させたいオブジェクトを指定し、そのオブジェクトでメソッドを実行する.ここではオブジェクトがNodeListのdocument.querySelectorAll('.open')で、NodeListに実行させたいメソッドがsliceである.
  // これによりNodeListはsliceメソッドが使えるようになり、openButtonsにはbuttonタグのArrayが入る.
  // sliceは引数で指定された範囲の要素を配列として返すが、引数を指定しなかった場合その配列を丸ごと返すので、NodeList等の配列のようなオブジェクトを配列に変換できる.
  // 参考: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
  // http://taiju.hatenablog.com/entry/20100515/1273903873
  const openButtons = Array.prototype.slice.call(document.querySelectorAll('.open'));
  const closeButtons = Array.prototype.slice.call(document.querySelectorAll('.close'));

  // winオブジェクト格納用の空オブジェクト
  let winObjs = {};

  openButtons.forEach((button) => {
    button.addEventListener('click', () => {
      // buttonタグのidをkeyに返り値のwinオブジェクトをvalueにしてwinObjsに格納
      const win = createNewWindow(button.value);
      winObjs[button.id] = win;
    });
  });

  closeButtons.forEach((button) => {
    button.addEventListener('click', () => {
      // buttonタグのidをkeyにwinオブジェクトをwinObjsから取得
      closeWindow(winObjs[button.id]);
    });
  });

  function createNewWindow(url) {
    return window.open(url, "新ウィンドウ", "width=400, height=600");
  }

  function closeWindow(win) {
    win.close();
  }
});
