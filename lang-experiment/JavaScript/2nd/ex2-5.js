'use strict';

document.addEventListener('DOMContentLoaded', function() {
  // querySelectorAllで返ってくるオブジェクトはNodeListなのでArrayのforEachやsliceメソッドが使えない.
  // その為callメソッドを使ってNodeListでArrayオブジェクトのsliceメソッドを使えるようにしている.
  // callは第一引数にメソッドを実行させたいオブジェクトを指定し、そのオブジェクトでメソッドを実行する.ここではオブジェクトがNodeListのdocument.querySelectorAll('.next')で、NodeListに実行させたいメソッドがsliceである.
  // これによりNodeListはsliceメソッドが使えるようになり、nextButtonsにはbuttonタグのArrayが入る.
  // sliceは引数で指定された範囲の要素を配列として返すが、引数を指定しなかった場合その配列を丸ごと返すので、NodeList等の配列のようなオブジェクトを配列に変換できる.
  // 参考: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
  // http://taiju.hatenablog.com/entry/20100515/1273903873
  let nextButtons = Array.prototype.slice.call(document.querySelectorAll('.next'));
  let prevButtons = Array.prototype.slice.call(document.querySelectorAll('.previous'));
  let top = document.querySelector('#top');


  nextButtons.forEach((button) => {
    // nextのclassを持つbuttonタグに次の関数をクリックイベントに追加
    button.addEventListener('click', () => {
      // 1ms毎に1px下に移動する
      let count = 0;
      let intervalID = setInterval(() => {
        window.scrollBy(0, 1);

        count++;
        if (count >= 500) clearInterval(intervalID);
      }, 1);
    });
  });

  prevButtons.forEach((button) => {
    button.addEventListener('click', () => {
      // 1ms毎に1px上に移動する
      let count = 0;
      let intervalID = setInterval(() => {
        window.scrollBy(0, -1);

        count++;
        if (count >= 500) clearInterval(intervalID);
      }, 1);
    });
  });

  // クリックされたら (0, 0)に移動する関数をクリックイベントに追加
  top.addEventListener('click', () => {
    window.scroll(0, 0);
  });
});
