'use strict';

document.addEventListener('DOMContentLoaded', function() {
  // () => {} は function() {}と同様の意味
  setInterval(() => {
    // divタグの背景情報を保持
    let color = document.querySelector('div').style.backgroundColor;
    // changeColor関数が次の色を返す
    document.querySelector('div').style.backgroundColor = changeColor(color);
  }, 3000);

  function changeColor(color) {
    switch (color) {
      case 'rgb(255, 0, 0)':
        return '#0000ff';
      case 'rgb(0, 0, 255)':
        return '#ffff00';
      case 'rgb(255, 255, 0)':
        return '#ff0000';
      default:
      return '#ff0000';
    }
  }
});
