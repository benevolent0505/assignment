"use strict"; // strictモードオン

// 定数宣言
const MAX = 50;

// DOMContentLoaded: DOMの構築が完了したときに発火するイベント
document.addEventListener('DOMContentLoaded', function() {
  // querySelector: 引数のCSSセレクタを検索するして一番始めの要素を返えす関数
  // 引数にはタグ名, id, classなどの指定が出来る
  let tbody = document.querySelector('tbody');

  for (let i = 0; i < MAX; i++) {
    // 値の設定
    let two = Math.floor(Math.random() * 8);
    let one = Math.floor(Math.random() * 8);
    let zero = Math.floor(Math.random() * 8);

    // trタグとtdタグの生成 (createElement)
    let tr = document.createElement('tr');
    let td2 = document.createElement('td');
    td2.textContent = two;
    let td1 = document.createElement('td');
    td1.textContent = one;
    let td0 = document.createElement('td');
    td0.textContent = zero;

    // trの末尾にtdタグを追加していく(appendChild)
    tr.appendChild(td2);
    tr.appendChild(td1);
    tr.appendChild(td0);

    // 値が全部等しい時にtrタグにredクラスを追加する
    if (two == one && one == zero) {
      tr.classList.add('red');
    }

    // trタグをtbodyタグの末尾に追加する
    tbody.appendChild(tr);
  }
});
