function scrclr () {
  for (let i=0; i<document.tstform.length; i++) {
    if (document.tstform.elements[i].type != "button") {
      document.tstform.elements[i].value = "";
    }
  }
  document.tstform.english.focus ( );
}
function cave () {
  const english = parseInt(document.tstform.english.value);
  const math = parseInt(document.tstform.mathmatics.value);

  // calcGradeで返ってきた文字列をdivタグのテキストにするl
  document.querySelector('#english-grade').textContent = calcGrade(english);
  document.querySelector('#math-grade').textContent = calcGrade(math);
}

// 点数に応じて文字列を返す
function calcGrade(mark) {
  if (mark >= 90) {
    return "秀（90点以上）";
  } else if (mark >= 80) {
    return "優（80-89点）";
  } else if (mark > 70) {
    return "良（70-79点）";
  } else if (mark > 60) {
    return "可（60-69点）";
  } else if (mark <= 59) {
    return "不可（59点以下）";
  } else {
    return "正しい値が入力されていません";
  }
}
