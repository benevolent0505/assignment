'use strict';

document.addEventListener('DOMContentLoaded', () => {
  // ボタンをクリックしたとき
  document.querySelector('#submit').addEventListener('click', () => {
    // ラジオボタンの配列を作り
    const radioButtons = Array.prototype.slice.call(document.querySelectorAll('input[name=color]'));

    // チェックの付いたラジオボタンを抜き出す(配列で返ってくるので[0]で取り出す)
    const checkedRadio = radioButtons.filter((radio) => {
      return radio.checked;
    })[0];

    // ラジオボタンのvalueの値をclassに代入して、cssで色を変える
    document.querySelector('#text').className = checkedRadio.value;
  });
});
