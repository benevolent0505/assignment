"use strict";

document.addEventListener('DOMContentLoaded', () => {
  // 読み込み時に選択されている画像を表示する
  selectImage(document.querySelector('#select-image').selectedOptions[0]);

  // 違うoption要素が選択されると、その要素からURLを取得し代入する
  document.querySelector('#select-image').addEventListener('change', () => {
    const option = document.querySelector('#select-image').selectedOptions[0];

    selectImage(option);
  });

  // option要素のvalueからURLを取得しimgタグのsrcに代入する関数
  function selectImage(option) {
    document.querySelector('#view-image').src = option.value;
  }
});
