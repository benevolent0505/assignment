'use strict';

document.addEventListener('DOMContentLoaded', () => {

  // 1秒毎に
  setInterval(() => {
    // ランダムな値を背景にする
    document.bgColor = Math.floor(Math.random() * 0xFFFFFF).toString(16);
  }, 1000);

  document.querySelector('#close').addEventListener('click', () => {
    window.close();
  });
});
