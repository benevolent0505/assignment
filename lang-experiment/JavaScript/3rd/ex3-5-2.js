'use strict';

document.addEventListener('DOMContentLoaded', () => {
  const limit = Date.parse("Jan 1, 2017");

  // 1秒毎に
  setInterval(() => {
    // 期限との差を取り
    let lapse = limit - Date.now();

    // 残り時間を求める
    const restDays = millisecToDay(lapse);
    document.querySelector('#days').textContent = restDays;
    lapse -= dayToMillisec(restDays);

    const restHour = millisecToHour(lapse);
    document.querySelector('#hour').textContent = restHour;
    lapse -= hourToMillisec(restHour);

    const restSec = millisecToSecond(lapse);
    document.querySelector('#second').textContent = restSec;
  }, 1000);

  document.querySelector('#close').addEventListener('click', () => {
    window.close();
  });

  // 以下は変換用の関数
  function millisecToDay(millisecond) {
    return Math.floor(millisecond/1000/60/60/24);
  }

  function dayToMillisec(day) {
    return day*1000*60*60*24;
  }

  function millisecToHour(millisecond) {
    return Math.floor(millisecond/1000/60/60);
  }

  function hourToMillisec(hour) {
    return hour*1000*60*60;
  }

  function millisecToSecond(millisecond) {
    return Math.floor(millisecond/1000);
  }
});
