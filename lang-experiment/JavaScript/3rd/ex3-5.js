"use strict";

document.addEventListener('DOMContentLoaded', () => {
  const links = document.querySelector('#links');
  links.addEventListener('change', () => {
    const option = links.selectedOptions[0];

    window.open(option.value);
  });
});
