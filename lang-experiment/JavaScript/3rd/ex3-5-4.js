'use strict';

const STORAGE_NAME = 'time_schedule';

// localstrage用の関数
const storage = {
  fetch: () => {
    return JSON.parse(localStorage.getItem(STORAGE_NAME));
  },
  save: (todos) => {
    localStorage.setItem(STORAGE_NAME, JSON.stringify(todos));
  }
};

// 時間割を表わすオブジェクト
class TimeSchedule {
  constructor() {
    this.timeSchedule = [
      [], [], [], [], [], [], []
    ];
  }
  setLesson(day, period, lesson) {
    if (!this.timeSchedule[day][period - 1]) {
      this.timeSchedule[day][period - 1] = lesson;
    }
  }
  getLesson(day, period) {
    return this.timeSchedule[day][period - 1];
  }
  deleteLesson(day, period) {
    if (this.timeSchedule[day][period-1]) {
      this.timeSchedule[day][period-1] = null;
    }
  }
  saveSchedule() {
    storage.save(this.timeSchedule);
  }
  loadSchedule() {
    this.timeSchedule =  storage.fetch() ? storage.fetch() : this.timeSchedule;
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const timeSchedule = new TimeSchedule();

  const table = document.querySelector('#time-table');
  const registButton = document.querySelector('#register');
  const deleteButton = document.querySelector('#delete');
  const saveButton = document.querySelector('#save');
  const loadButton = document.querySelector('#load');

  timeSchedule.loadSchedule();
  renderTable();

  registButton.addEventListener('click', () => {
    const day = document.querySelector('#day').value;
    const period = document.querySelector('#period').value;
    const lesson = document.querySelector('#lesson').value;

    timeSchedule.setLesson(day, period, lesson);

    renderTable();
  });

  deleteButton.addEventListener('click', () => {
    const day = document.querySelector('#delete-day').value;
    const period = document.querySelector('#delete-period').value;

    timeSchedule.deleteLesson(day, period);

    renderTable();
  });

  saveButton.addEventListener('click', () => {
    timeSchedule.saveSchedule();
  });

  loadButton.addEventListener('click', () => {
    timeSchedule.loadSchedule();

    renderTable();
  });

  // 時間割表示用の関数
  function renderTable() {
    for (let i = 1; i < table.rows.length; i++) {
      for (let j = 0; j < table.rows[i].cells.length; j++) {
        const lessonName = timeSchedule.getLesson(j,i);

        if (lessonName) {
          table.rows[i].cells[j].textContent = lessonName;
        } else {
          table.rows[i].cells[j].textContent = "--";
        }
      }
    }
  }

  document.querySelector('#close').addEventListener('click', () => {
    window.close();
  });
});
