"use strict";

document.addEventListener('DOMContentLoaded', () => {

  // submitボタンが押された時に
  document.querySelector('#submit').addEventListener('click', () => {
    // idとpassを取得
    const id = document.querySelector('#id').value;
    const pass = document.querySelector('#password').value;

    // 一致すればIEDのページにリダイレクト、してなければalertを出す
    if (id == pass) {
      location.href = "http://www.ied.inf.uec.ac.jp/";
    } else {
      alert("Password incorrect");
    }
  });
});
