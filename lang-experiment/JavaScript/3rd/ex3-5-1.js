'use strict';

document.addEventListener('DOMContentLoaded', () => {
  const bookmarks = document.querySelector('#bookmarks');

  // bookmarksのラジオボタンが選択されたら
  bookmarks.addEventListener('change', () => {
    const option = bookmarks.selectedOptions[0];

    // そのvalueのURLを開く
    window.open(option.value);
  });

  // クローズボタンがクリックされたらウィンドウを閉じる
  document.querySelector('#close').addEventListener('click', () => {
    window.close();
  });
});
