'use strict';

const colors = ['red', 'blue', 'black'];

// Game object
class Game {
  constructor() {
    this.redCount = 0;
    this.blueCount = 0;
  }
  getRedCount() {
    return this.redCount;
  }
  getBlueCount() {
    return this.blueCount;
  }
  incRedCount() {
    this.redCount++;
  }
  incBlueCount() {
    this.blueCount++;
  }
}

document.addEventListener('DOMContentLoaded', () => {
  // values
  const startButton = document.querySelector('#start');
  const buttons = Array.prototype.slice.call(document.querySelectorAll('span[name=button]'));
  let challengeCount = 20;
  const game = new Game();

  setTextContent('challenge-count', challengeCount);
  // 各ボタンにカウント変更のイベントを追加
  buttons.forEach((button) => {
    button.addEventListener('click', () => {
      challengeCount += getChallengeNumber(button.className);
      setTextContent('challenge-count', challengeCount);
    });
  });

  // set event listener
  startButton.addEventListener('click', () => {
    challengeCount = 20;
    const intervalID = setInterval(() => {
      challengeCount -= 1;

      // 色変更部分
      let useRed = false;
      let useBlue = false;
      buttons.forEach((button) => {
        let color = colors[Math.floor(Math.random() * colors.length)];

        if (color == 'red') {
          color = useRed ? 'black' : color;
          useRed = true;
        } else if (color == 'blue') {
          color = useBlue ? 'black' : color;
          useBlue = true;
        }
        button.className = color;
      });

      // update challenge count
      setTextContent('challenge-count', challengeCount);

      // finish process
      if (challengeCount <= 0) {
        alert("あなたの得点は" + game.getRedCount()
              + "\n(赤クリック回数" + game.getRedCount()
              + ", 青クリック回数" + game.getBlueCount() + ")");

        clearInterval(intervalID);
      }
    }, 1000);
  });

  // utility functions
  function setTextContent(tagID, text) {
    document.getElementById(tagID).textContent = text;
  }

  function getChallengeNumber(value) {
    switch (value) {
    case 'red':
      game.incRedCount();
      return 2;
    case 'blue':
      game.incBlueCount();
      return -3;
    default:
      return 0;
    }
  }
});
