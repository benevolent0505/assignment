#include <stdio.h>
#include <stdlib.h>

#define N 1000
typedef struct __ListAry {
  int ary[N];
  int nitems;
} ListAry;

void show_list(ListAry *p);
int get_item(int pos, ListAry *p);
void insert(int x, int pos, ListAry *p);
void delete (int pos, ListAry *p);
int first(ListAry *p);
int last(ListAry *p);
int next(int pos, ListAry *p);
int prev(int pos, ListAry *p);
int locate(int i, ListAry *p);

int main(void) {
  ListAry a;
  int i, n;

  n = 10;
  for (int i = 1; i <= n; i++) {
    insert(i * i, i - 1, &a);
  }

  // show the list
  show_list(&a);
  printf("\n");

  // check first() and last()
  printf("first item: %d\n", get_item(first(&a), &a));
  printf("last item: %d\n", get_item(last(&a), &a));
  printf("\n");

  // check next(), first()
  printf("search forward\n");

  for (int i = first(&a); i != -1; i = next(i, &a)) {
    printf("%d\n", get_item(i, &a));
  }
  printf("\n");

  // check last(), prev()
  printf("search backward\n");
  for (int i = last(&a); i != -1; i = prev(i, &a)) {
    printf("%d\n", get_item(i, &a));
  }
  printf("\n");

  // check locate()
  printf("3rd item = %d\n", get_item(locate(3, &a), &a));

  if ((i = locate(100, &a)) != -1) {
    printf("100th item = %d\n", get_item(i, &a));
  } else {
    printf("No such item\n");
  }
  printf("\n");

  // check delete ()
  printf("delete 5th item\n");
  i = locate(5, &a);
  delete (i, &a);
  show_list(&a);
  printf("\n");

  return 0;
}

void insert(int x, int pos, ListAry *p) {
  if (p->nitems < N) {
    if (pos <= p->nitems) {
      for (int i = p->nitems - 1; i >= pos + 1; i--) {
        p->ary[i + 1] = p->ary[i];
      }
      p->ary[pos] = x;
      p->nitems++;
    }
  }
}

void delete (int pos, ListAry *p) {
  if (p->nitems > 0) {
    if (pos >= 0 && pos < p->nitems - 1) {


      for (int i = pos; i < p->nitems - 1; i++) {
        p->ary[i] = p->ary[i + 1];
      }
      p->nitems--;
    }
  }
}

int first(ListAry *p) {
  int ret = -1;
  if (p->nitems > 0) ret = 0;

  return ret;
}

int last(ListAry *p) {
  int ret = -1;
  if (p->nitems > 0) ret = p->nitems - 1;

  return ret;
}

int next(int pos, ListAry *p) {
  int ret = -1;

  if (pos + 1 >= 0 && pos + 1 < p->nitems) {
    ret = pos + 1;
  }

  return ret;
}

int prev(int pos, ListAry *p) {
  int ret = -1;

  if (pos - 1 >= 0 && pos - 1 < p->nitems) {
    ret = pos - 1;
  }

  return ret;
}

int locate(int i, ListAry *p) {
  int ret = -1;

  if (i >= 0 && i < p->nitems) {
    ret = i;
  }

  return ret;
}

int get_item(int pos, ListAry *p) { return (p->ary[pos]); }

void show_list(ListAry *p) {
  int i;
  for (i = 0; i < p->nitems; i++) {
    printf("%d\n", get_item(i, p));
  }
}
