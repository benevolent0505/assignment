/*
  課題4-1
  提出日 2015/05/18
  学籍番号 1410114
  氏名 藤田 幹央
  感想: 古いCで書くのやめて、これからはC99を使って書いていきます。
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

Node *insert(int x, Node *p);
Node *first(Node *head);
Node *last(Node *head);
Node *next(Node *p);
Node *prev(Node *p, Node *head);
Node *locate(int n, Node *head);
void delete (Node *p);

int main() {
  int i, n;
  Node *p, *head;

  p = head = NULL;
  for (i = 1; i <= 10; i++) {
    p = insert(i * i, p);  // 1. 二乗の値のリストへの保持
    if (head == NULL) head = p;
  }

  p = first(head);  // 2. 最初の要素の表示
  printf("最初の要素は: %d\n", p->data);

  p = last(head);  // 2. 最後の要素の表示
  printf("最後の要素は: %d\n", p->data);

  // 3. 順に表示
  for (p = first(head); p != NULL; p = next(p)) {
    printf("item => %d\n", p->data);
  }

  // 4. 逆順に表示
  for (p = last(head); p != NULL; p = prev(p, head)) {
    printf("item => %d\n", p->data);
  }

  // 5. 特定のノード(5番目)の消去
  n = 4;  // 4 番目の次を消去するので
  p = locate(n, head);
  delete (p);
  for (p = first(head); p != NULL; p = next(p)) {
    printf("item => %d\n", p->data);
  }

  return 0;
}

Node *insert(int x, Node *p) {
  Node *insert = malloc(sizeof(Node));
  insert->data = x;

  if (p != NULL) {
    insert->next = p->next;
    p->next = insert;
  }

  return insert;
}

Node *first(Node *head) { return head; }

Node *last(Node *head) {
  Node *last = head;

  while (last->next != NULL) {
    last = last->next;
  }

  return last;
}

Node *next(Node *p) { return p->next; }

Node *prev(Node *p, Node *head) {
  if (p == NULL || p == head) return NULL;

  Node *prev = head;

  while (prev->next != p) {
    prev = prev->next;
  }

  return prev;
}

Node *locate(int n, Node *head) {
  if (head == NULL) return NULL;

  Node *p = head;
  for (int i = 1; i < n; i++) {
    p = p->next;
  }

  return p;
}

/* 指定したNodeの次のNodeを削除する仕様なのでこの実装としている */
void delete (Node *p) {
  if (p != NULL) {
    Node *q = p->next->next;

    p->next = q;

    free(p->next);
  }
}

/*
  最初の要素は: 1
  最後の要素は: 100
  item => 1
  item => 4
  item => 9
  item => 16
  item => 25
  item => 36
  item => 49
  item => 64
  item => 81
  item => 100
  item => 100
  item => 81
  item => 64
  item => 49
  item => 36
  item => 25
  item => 16
  item => 9
  item => 4
  item => 1
  item => 1
  item => 4
  item => 9
  item => 16
  item => 36
  item => 49
  item => 64
  item => 81
  item => 100
 */
