#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

// global variable
Node *head;

// functions
void show_list( Node *p );
Node* setup_list( int n );
void delete(Node *p);
void deleteNext(Node *p);

int main() {
  Node *p, *q;
  int n;

  n = 10;
  head = setup_list(n);
  // リストの表示
  show_list(head);
  printf("\n");

  // 3番目の要素の削除
  q = head -> next;  // q で削除場所を指定

  deleteNext(q);

  /* delete(q); */
  /* p = q -> next; */

  /* q -> next = q -> next -> next; // q->next を削除 */

  /* free(p); */


  // リストの表示
  show_list( head );

  return 0;
}

void show_list(Node *p) {
  for ( ; p != NULL; p = p -> next) {
    printf("%d\n", p -> data);
  }
}

// listの先頭を返す
Node* setup_list(int n) {
  int i;
  Node *head = NULL;
  Node *p;

  for (i = 0; i < n; i++) {
    if (head == NULL) {
      p = malloc(sizeof(Node));
      head = p;
    } else {
      p -> next = malloc(sizeof(Node));
      p = p -> next;
    }

    p -> data = i;
    p -> next = NULL; // 最後のノードの印
  }

  return head;
}

/* p で指定された要素を削除 */
void delete(Node *p) {
  if (p != NULL) {
    Node *q = head;

    for ( ; q -> next != p; q = q -> next) {}

    q -> next = p -> next;

    free(p);
  }
}

/* p で指定された要素の次の要素を削除 */
void deleteNext(Node *p) {
  if (p != NULL) {
    Node *q = p -> next -> next;

    p -> next = q;

    free(p -> next);
  }
}
