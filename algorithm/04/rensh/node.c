#include <stdio.h>

typedef struct __Node{
  int data;
  struct __Node *next; // 自分自身の型へのポインタ(自己参照型構造体)
} Node;

int main() {
  Node a, b;

  a.data = 1;
  b.data = 2;
  a.next = &b; // a の次に b をつなぐ

  printf("a.data: %d\n", a.data);
  printf("b.data: %d\n", b.data);
  printf("a.next->data: %d\n", a.next->data);

  return 0;
}
