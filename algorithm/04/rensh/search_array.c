#include <stdio.h>

#define N 100
#define TRUE 1
#define FALSE 0

int main() {
  int a[N];
  int n;
  int i, isFind;
  int x;     // search data

  n = 10;
  for (i = 0; i < n; i++) {
    a[i] = i;
  }

  x = 100;  // 探すべきデータ
  isFind = FALSE;
  for (i = 0; i < n; i++) {
    if (a[i] == x) {
      isFind = TRUE;
      break;
    }
  }

  if (isFind) {
    printf("I found %d in the list\n", x);
  } else {
    printf("I cannot find %d in the list\n", x);
  }

  return 0;
}
