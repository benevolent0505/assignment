#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

void show_list(Node *p);
Node* setup_list(int n);

int main() {
  Node *head, *p;
  int i, k, n;

  head = NULL;
  n = 10;
  head = setup_list(n);

  // get k-th item
  p = head;
  k = 4;
  for (i = 0; i < k-1; i++){
    p = p -> next;
  }

  printf("k-th item: %d\n", p -> data);

  return 0;
}

void show_list(Node *p) {
  for ( ; p != NULL ;p = p -> next) {
    printf("%d\n", p -> data);
  }
}

// listの先頭を返す
Node* setup_list(int n) {
  int i;
  Node *head = NULL;
  Node *p;

  for (i = 0; i < n; i++) {
    if (head == NULL) {
      p = malloc(sizeof(Node));
      head = p;
    } else {
      p -> next = malloc(sizeof(Node));
      p = p -> next;
    }

    p -> data = i;
    p -> next = NULL; // 最後のノードの印
  }

  return head;
}
