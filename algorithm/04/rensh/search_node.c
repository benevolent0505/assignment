#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

Node* setup_list(int n);

int main() {
  Node *head, *p;
  int n, x;
  int isFind;

  head = NULL;
  n = 10;
  head = setup_list(n);

  // 探索するデータ
  x = 100;
  isFind = FALSE;

  for ( p = head; p != NULL; p = p -> next) {
    if (p -> data == x) {
      isFind = TRUE;
      break;
    }
  }

  if (isFind) {
    printf("I found %d in the list\n", x);
  } else {
    printf("I cannot find %d in the list\n", x);
  }

  return 0;
}

/* listの先頭を返す */
Node* setup_list(int n) {
  int i;
  Node *head = NULL;
  Node *p;

  for (i = 0; i < n; i++) {
    if (head == NULL) {
      p = malloc(sizeof(Node));
      head = p;
    } else {
      p -> next = malloc(sizeof(Node));
      p = p -> next;
    }

    p -> data = i;
    p -> next = NULL; // 最後のノードの印
  }

  return head;
}
