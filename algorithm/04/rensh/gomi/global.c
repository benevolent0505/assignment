#include <stdio.h>

int head = 0;

void printHead() {
  printf("Head : %d\n", head);
}

int main() {

  printHead();

  head = 10;

  printf("Head : %d\n", head);

  printHead();

  return 0;
}
