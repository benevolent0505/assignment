#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

void show_list(Node *p);
Node* setup_list(int n);
Node* insert(int x, Node *p);

int main() {
  Node *head, *p, *q;
  int n;

  n = 10;
  head = setup_list(n);
  // リストの表示
  show_list(head);
  printf("\n");

  // 10 を 2番目の要素に挿入
  q = head -> next -> next;  // q で挿入場所の指定

  /* p = malloc(sizeof(Node)); // 挿入要素 p の準備 */
  /* p -> data = 10; */

  /* // q の後ろに p を挿入 */
  /* p -> next = q -> next; */
  /* q -> next = p; */

  insert(10, q);

  // リストの表示
  show_list(head);

  return 0;
}

void show_list(Node *p) {
  for ( ; p != NULL; p = p -> next) {
    printf("%d\n", p -> data);
  }
}

// 作ったlistの先頭を返す
Node* setup_list(int n) {
  int i;
  Node *head = NULL;
  Node *p;

  for (i = 0; i < n; i++) {
    if (head == NULL) {
      p = malloc(sizeof(Node));
      head = p;
    } else {
      p -> next = malloc(sizeof(Node));
      p = p -> next;
    }

    p -> data = i;
    p -> next = NULL; // 最後のノードの印
  }

  return head;
}

/* x を Node *pの後ろに挿入 */
Node* insert(int x, Node *p) {
  Node *insert  = insert = malloc(sizeof(Node));
  insert -> data = x;

  if (p != NULL) {
    insert -> next = p -> next;
    p -> next = insert;
  }

  return insert;
}
