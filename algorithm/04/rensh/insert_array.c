#include <stdio.h>

#define N 100

int main() {
  int a[N];
  int n;
  int i;

  n = 10;
  for(i = 0; i < n; i++) {
    a[i] = i;
  }
  // リストの表示
  for(i = 0; i < n; i++) {
    printf( "item[%d] = %d\n", i, a[i]);
  }
  printf("\n");


  // リストへの挿入．a[5] に `10' を入れたい場合
  // a[5] 以降をひとつづつずらして保存
  // move a[n-1] => a[n]
  //      a[n-2] => a[n-1]
  //             :
  //      a[5] => a[6]
  for(i = n - 1; i >= 5; i--) {
    a[i+1] = a[i];
  }
  a[5] = 10;
  n += 1; // 数が増えたので(本来は n <= N のチェックがいる)

  // リストの表示
  for(i = 0; i < n; i++) {
    printf( "item[%d] = %d\n", i, a[i]);
  }

  return 0;
}
