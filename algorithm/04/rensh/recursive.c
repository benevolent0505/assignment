#include <stdio.h>

long fact(int n);

int main() {
  int n;
  long f;

  printf("整数を入力して下さい\t");
  scanf("%d", &n);

  f = fact(n);
  printf("Factorial2(  ) = %ld\n", f);

  return 0;
}

long fact(int n) {
    if (n == 0 || n == 1)        /* nが0か1なら */
        return (1L);             /* 1を返す */
    else                         /* nに自分より1小さい自分を掛ける */
        return (n * fact(n -1));
}
