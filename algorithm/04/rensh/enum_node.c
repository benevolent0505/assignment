#include <stdio.h>
#include <stdlib.h>

typedef struct __Node{
  int data;
  struct __Node *next;
} Node;


int main() {
  Node *head, *p;

  p = malloc(sizeof(Node));
  head = p; // リストの先頭を`head'に保存
  p -> data = 1;

  p -> next = malloc(sizeof(Node));
  p = p -> next;
  p -> data = 2;

  p -> next = malloc(sizeof(Node));
  p = p -> next;
  p -> data = 3;

  p -> next = NULL; // p の後ろがこれ以上ないので


  // リストの表示
  for (p = head; p != NULL; p = p -> next) {
    printf("p->data: %d\n", p -> data);
  }

  return 0;
}
