#include <stdio.h>
#define N 50

int main() {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // heap size
  int parent, child, tmp;

  a[0] = 30;  //ヒープ
  a[1] = 10;
  a[2] = 20;
  a[3] = 4;
  a[4] = 7;

  n = 5;  //ヒープの最後

  printf("input data: ");  // input data
  scanf("%d", &a[n]);      //ヒープの最後に追加

  child = n;                 //追加ノードを子ノード
  parent = (child - 1) / 2;  //その親ノード
  while (child > 0 && a[parent] < a[child]) {  //根まで比較
    tmp = a[parent];                           //交換
    a[parent] = a[child];
    a[child] = tmp;
    child = parent;            //親ノードを子ノードに
    parent = (child - 1) / 2;  //その親ノード
  }

  for (int i = 0; i <= n; i++) {
    printf("a[%d] = %d \n", i, a[i]);
  }

  return 0;
}
