#include <stdio.h>
#include <stdlib.h>

#define N 50

void swap(int *a, int *b);

int main() {
  int a[N];
  int n;
  int parent, child;

  // 2分木
  a[0] = 8;
  a[1] = 4;
  a[2] = 6;
  a[3] = 5;
  a[4] = 3;
  a[5] = 9;
  a[6] = 2;
  a[7] = 1;
  a[8] = 7;

  n = 8;

  for (int j = 0; j <= n; j++) {
    printf("a[%d] = %d\n", j, a[j]);
  }

  for (int i = (n - 1) / 2; i >= 0;
       i--) {  //最後の部分木から根を親とする部分木まで
    parent = i;
    child = parent * 2 + 1;  //左の子ノード

    while (child <= n) {  //下方移動
      if (child < n && a[child] > a[child + 1]) {
        child++;
      }

      if (a[parent] > a[child]) {  //親ノードが小さい場合
        swap(&a[parent], &a[child]);
      }
      parent = child;          //次の親ノード設定
      child = parent * 2 + 1;  //その左の子ノード設定
    }

    printf("\n");
    for (int j = 0; j <= n; j++) {
      printf("a[%d] = %d\n", j, a[j]);
    }
  }

  return 0;
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}
