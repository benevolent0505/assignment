#include <stdio.h>
#include <stdlib.h>
#define N 50

void swap(int *a, int *b);

int main() {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int parent, child;

  a[0] = 8;
  a[1] = 4;
  a[2] = 6;
  a[3] = 5;
  a[4] = 3;
  a[5] = 9;
  a[6] = 2;
  a[7] = 1;
  a[8] = 7;

  n = 8;

  for (int j = 0; j <= n; j++) {
    printf("a[%d]=%d  ", j, a[j]);
  }
  printf("\n");

  for (int i = (n - 1) / 2; i >= 0; i--) {
    parent = i;
    child = parent * 2 + 1;
    while (child <= n) {
      if (child < n && a[child] > a[child + 1]) child++;

      if (a[parent] > a[child]) {
        swap(&a[parent], &a[child]);
      }
      parent = child;
      child = parent * 2 + 1;
    }
    for (int j = 0; j <= n; j++) {
      printf("a[%d]=%d  ", j, a[j]);
    }
    printf("\n");
  }

  return 0;
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}
