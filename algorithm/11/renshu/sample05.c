#include <stdio.h>
#include <stdlib.h>
#define N 50

void shiftdown(int, int, int *);
void swap(int *a, int *b);

int main() {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int m, parent, child;

  a[0] = 8;
  a[1] = 4;
  a[2] = 6;
  a[3] = 5;
  a[4] = 3;
  a[5] = 9;
  a[6] = 2;
  a[7] = 1;
  a[8] = 7;

  n = 8;

  for (int j = 0; j <= n; j++) printf("a[%d]=%d  ", j, a[j]);
  printf("\n");

  //ヒープの構成
  for (int i = (n - 1) / 2; i >= 0; i--) {
    parent = i;
    child = parent * 2 + 1;
    while (child <= n) {
      if (child < n && a[child] < a[child + 1]) child++;

      if (a[parent] < a[child]) {
        swap(&a[parent], &a[child]);
      }
      parent = child;
      child = parent * 2 + 1;
    }

    for (int j = 0; j <= n; j++) printf("a[%d]=%d  ", j, a[j]);
    printf("\n");
  }
  m = n;
  printf("===================");
  printf("\n");

  while (m > 0) {  //根の削除とヒープの再構成
    a[0] = a[m];
    m--;
    shiftdown(0, m, a);

    for (int j = 0; j <= m; j++) printf("a[%d]=%d  ", j, a[j]);
    printf("\n");
  }
  return 0;
}

void shiftdown(int parent, int n, int heap[]) {
  int child;

  child = parent * 2 + 1;
  // 下方移動
  while (child <= n) {
    if (child < n && heap[child] < heap[child + 1]) child++;

    if (heap[parent] < heap[child]) {
      swap(&heap[parent], &heap[child]);
    }
    parent = child;
    child = parent * 2 + 1;
  }
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}
