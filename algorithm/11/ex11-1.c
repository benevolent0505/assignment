/**
 * 課題11-1
 * 提出日 2015/07/04
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: ヒープの値の入力の処理をもう少しうまく出来ないかと悩んでいる.
 */
#include <stdio.h>
#define N 50

int getParent(int child);

int setHeapValue(int heap[], int length);
void dumpHeap(int heap[], int length);
void swap(int *a, int *b);

int main() {
  int heap[N];

  int heapLength = 0;
  int parent, child;

  // ヒープの構成
  while (1) {
    child = heapLength;
    parent = getParent(child);

    printf("input data: ");
    int res = setHeapValue(heap, heapLength);
    if (res == -1) {
      heapLength--;
      break;
    }

    // 上方移動により構成
    while (child > 0 && heap[parent] < heap[child]) {
      swap(&heap[parent], &heap[child]);

      child = parent;
      parent = getParent(child);
    }

    heapLength++;
    dumpHeap(heap, heapLength);

    if (heapLength > N) break;
  }

  return 0;
}

void dumpHeap(int heap[], int length) {
  for (int i = 0; i < length; i++) {
    printf("heap[%d] = %d", i, heap[i]);
    if (i != length) printf(" ");
  }

  printf("\n");
}
int getParent(int child) { return (child - 1) / 2; }

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}

int setHeapValue(int heap[], int length) {
  int tmp;

  int res = scanf("%d", &tmp);
  if (res == EOF) {
    return -1;
  } else {
    heap[length] = tmp;
  }

  return 0;
}
/**
 * input data: 1
 * heap[0] = 1
 * input data: 2
 * heap[0] = 2 heap[1] = 1
 * input data: 3
 * heap[0] = 3 heap[1] = 1 heap[2] = 2
 * input data: 4
 * heap[0] = 4 heap[1] = 3 heap[2] = 2 heap[3] = 1
 * input data: 5
 * heap[0] = 5 heap[1] = 4 heap[2] = 2 heap[3] = 1 heap[4] = 3
 * input data: 6
 * heap[0] = 6 heap[1] = 4 heap[2] = 5 heap[3] = 1 heap[4] = 3 heap[5] = 2
 * input data: 7
 * heap[0] = 7 heap[1] = 4 heap[2] = 6 heap[3] = 1 heap[4] = 3 heap[5] = 2 heap[6] = 5
 * input data: 8
 * heap[0] = 8 heap[1] = 7 heap[2] = 6 heap[3] = 4 heap[4] = 3 heap[5] = 2 heap[6] = 5 heap[7] = 1
 * input data: 9
 * heap[0] = 9 heap[1] = 8 heap[2] = 6 heap[3] = 7 heap[4] = 3 heap[5] = 2 heap[6] = 5 heap[7] = 1 heap[8] = 4
 * input data: ⏎
 */
