#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define  NO    1  /* 番号（入力用）*/
#define  NAME  2  /* 氏名（入力用）*/

int MaxLevel;
int CurrentLevel;
typedef enum {
  Term, Insert, Search, Dump, Dump_R
} Menu;

/*--- 会員データ ---*/
typedef struct {
  int   no;         /* 番号 */
  char  name[10];   /* 氏名(キー) */
} Data;

/*--- ノード ---*/
typedef struct  __bnode {
  Data             data;   /* データ */
  struct  __bnode  *left;  /* 左の子ノードへのポインタ */
  struct  __bnode  *right; /* 右の子ノードへのポインタ */
} BinNode;


int NameCmp(Data x, Data y);
int NumCmp(Data x, Data y);
BinNode *AllocNode(void);
void SetBinNode(BinNode *n, Data x, BinNode *left, BinNode *right);

BinNode *SearchNode(BinNode *p, Data x);
BinNode *InsertNode(BinNode *p, Data x);

Menu SelectMenu();
Data Read(char *message, int sw);
void PrintData (Data x);
void PrintTree(BinNode *p);
void PrintTreeR(BinNode *p);

void FreeTree (BinNode *p);
void initLevel();

int main() {
  Menu menu;
  BinNode *root = NULL;

  do {
    Data data;
    BinNode *tmp;

    menu = SelectMenu();

    switch (menu) {
    case Insert:
      initLevel();

      data = Read("挿入", NO | NAME);
      root = InsertNode(root, data);
      break;
    case Search:
      data = Read("探索", NAME);
      tmp = SearchNode(root, data);

      if (tmp != NULL) {
        PrintData(tmp -> data);
      } else {
        puts("みつかりませんでした(探索失敗)");
      }
      break;
    case Dump:
      puts("【一覧表】");
      PrintTree(root);
      break;
    case Dump_R:
      puts("【一覧表】");
      PrintTreeR(root);
      break;
    case Term:
      FreeTree(root);
      break;
    }
  } while (menu != Term);

  return 0;
}


/*--- データの氏名を比較 ---*/
int NameCmp(Data x, Data y) {
  return (strcmp(x.name, y.name));
}

int NumCmp(Data x, Data y) {
  if (x.no < y.no) {
    return -1;
  } else if (x.no > y.no) {
    return 1;
  }

  return 0;
}

/*--- 一つのノードを動的に確保 ---*/
BinNode *AllocNode(void) {
  return ((BinNode*)calloc(1, sizeof(BinNode)));
}

/*--- ノードの各メンバに値を設定 ---*/
void SetBinNode(BinNode *n, Data x, BinNode *left, BinNode *right) {
  n->data = x;      /* データ */
  n->left = left;   /* 左の子コードへのポインタ */
  n->right = right; /* 右の子ノードへのポインタ */
}

/*--- ノードの挿入 ---*/
BinNode *InsertNode(BinNode *p, Data x) {
  int cond;

  if (p == NULL) {
    if (MaxLevel < CurrentLevel) MaxLevel = CurrentLevel;

    p = AllocNode();
    SetBinNode(p, x, NULL, NULL);
    printf("現在のレベル = %d, 最大のレベル = %d\n", CurrentLevel, MaxLevel);
  } else if ((cond = NumCmp(x, p->data)) == 0) {
    printf("【エラー】%sは既に登録されています。\n", x.name);
  } else if (cond < 0) {
    CurrentLevel++;
    p->left = InsertNode(p->left, x);
  } else {
    CurrentLevel++;
    p->right = InsertNode(p->right, x);
  }

  return p;
}

/*--- 氏名による探索 ---*/
BinNode *SearchNode(BinNode *p, Data x) {
  int  cond;

  if (p == NULL)
    return (NULL);            /* 探索失敗 */
  else if ((cond = NumCmp(x, p->data)) == 0)
    return p;               /* 探索成功 */
  else if (cond < 0)
    return SearchNode(p->left, x);   /* 左部分木からの探索 */
  else
    return SearchNode(p->right, x);  /* 右部分木からの探索 */
}

/*--- データの番号と氏名を表示 ---*/
void PrintData (Data x) {
  printf("番号：%d  氏名：%s\n", x.no, x.name);
}

void PrintTree(BinNode *p) {
  if (p != NULL) {
    PrintTree(p->left);
    PrintData(p->data);
    PrintTree(p->right);
  }
}

void PrintTreeR(BinNode *p) {
  if (p != NULL) {
    PrintTreeR(p -> right);
    PrintData(p -> data);
    PrintTreeR(p -> left);
  }
}

/*--- 全ノードを開放 ---*/
void FreeTree (BinNode *p) {
  if (p != NULL){
    FreeTree(p->left);
    FreeTree(p->right);
    free(p);
  }
}

Menu SelectMenu() {
  int ch;

  do {
    printf("(1) 挿入 (2) 探索 (3) 表示 (4) 表示(逆順) (0) 終了：");
    scanf("%d", &ch);
  } while (ch < Term || ch > Dump_R);

  return (Menu)ch;
}

Data Read(char *message, int sw) {
  Data temp;

  printf("%sするデータを入力してください\n", message);

  if (sw & NO) { printf("番号：");  scanf("%d", &temp.no); }
  if (sw & NAME) { printf("名前：");  scanf("%s", temp.name); }

  return temp;
}

void initLevel() {
  CurrentLevel = 0;
}
