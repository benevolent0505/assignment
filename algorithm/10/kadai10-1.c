/**
 * 課題10-1
 * 提出日 2015/07/01
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: DeleteNodeで2回SearchNodeと同じことをしているのが気持ち悪いのでどうにかしたい。
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define  NO    1  /* 番号（入力用）*/
#define  NAME  2  /* 氏名（入力用）*/

typedef enum {
  Term, Insert, Search, Dump, Delete
} Menu;

/*--- 会員データ ---*/
typedef struct {
  int   no;         /* 番号 */
  char  name[10];   /* 氏名(キー) */
} Data;

/*--- ノード ---*/
typedef struct  __bnode {
  Data             data;   /* データ */
  struct  __bnode  *left;  /* 左の子ノードへのポインタ */
  struct  __bnode  *right; /* 右の子ノードへのポインタ */
} BinNode;


int NameCmp(Data x, Data y);
BinNode *AllocNode(void);
void SetBinNode(BinNode *n, Data x, BinNode *left, BinNode *right);

BinNode *SearchNode(BinNode *p, Data x);
BinNode *InsertNode(BinNode *p, Data x);

int DeleteNode(BinNode **root, Data x);

Menu SelectMenu();
Data Read(char *message, int sw);
void PrintData (Data x);
void PrintTree(BinNode *p);

void FreeTree (BinNode *p);

int main() {
  Menu menu;
  BinNode *root = NULL;

  do {
    Data data;
    BinNode *tmp;

    menu = SelectMenu();

    switch (menu) {
    case Insert:
      data = Read("挿入", NO | NAME);
      root = InsertNode(root, data);
      break;
    case Search:
      data = Read("探索", NAME);
      tmp = SearchNode(root, data);

      if (tmp != NULL) {
        PrintData(tmp -> data);
      } else {
        puts("みつかりませんでした(探索失敗)");
      }
      break;
    case Dump:
      puts("【一覧表】");
      PrintTree(root);
      break;
    case Delete:
      data = Read("削除", NAME);
      if (DeleteNode(&root, data) != -1) printf("削除しました。\n");
      break;
    case Term:
      FreeTree(root);
      break;
    }
  } while (menu != Term);

  return 0;
}


/*--- データの氏名を比較 ---*/
int NameCmp(Data x, Data y) {
  return (strcmp(x.name, y.name));
}

int NumCmp(Data x, Data y) {
  if (x.no < y.no) {
    return -1;
  } else if (x.no > y.no) {
    return 1;
  }

  return 0;
}

/*--- 一つのノードを動的に確保 ---*/
BinNode *AllocNode(void) {
  return ((BinNode*)calloc(1, sizeof(BinNode)));
}

/*--- ノードの各メンバに値を設定 ---*/
void SetBinNode(BinNode *n, Data x, BinNode *left, BinNode *right) {
  n->data = x;      /* データ */
  n->left = left;   /* 左の子コードへのポインタ */
  n->right = right; /* 右の子ノードへのポインタ */
}

/*--- ノードの挿入 ---*/
BinNode *InsertNode(BinNode *p, Data x) {
  int cond;

  if (p == NULL) {
    p = AllocNode();
    SetBinNode(p, x, NULL, NULL);
  } else if ((cond = NameCmp(x, p->data)) == 0) {
    printf("【エラー】%sは既に登録されています。\n", x.name);
  } else if (cond < 0) {
    p->left = InsertNode(p->left, x);
  } else {
    p->right = InsertNode(p->right, x);
  }

  return p;
}

/*--- 氏名による探索 ---*/
BinNode *SearchNode(BinNode *p, Data x) {
  int  cond;

  if (p == NULL) {
    return (NULL);            /* 探索失敗 */
  } else if ((cond = NameCmp(x, p->data)) == 0) {
    return p;               /* 探索成功 */
  } else if (cond < 0) {
    return SearchNode(p->left, x);   /* 左部分木からの探索 */
  } else {
    return SearchNode(p->right, x);  /* 右部分木からの探索 */
  }
}

int DeleteNode(BinNode **root, Data x) {
  BinNode *next, *tmp;
  BinNode **right;
  BinNode **p = root;

  while (1) {
    int cond;
    if (*p == NULL) {
      printf("【エラー】%sは登録されていません。\n", x.name);
      return -1;
    } else if ((cond = NameCmp(x,(*p) -> data)) == 0) {
      break;
    } else if (cond < 0) {
      p = &((*p) -> left);
    } else {
      p = &((*p) -> right);
    }
  }

  if ((*p) -> right == NULL) {
    next = (*p) -> left;
  } else {
    next = (*p) -> right;
    right = &((*p) -> right);
    while ((*right) -> left != NULL) right = &((*p) -> left);
    (*right) -> left = (*p) -> left;
  }

  tmp = *p;
  *p = next;
  free(tmp);

  return 0;
}

/*--- データの番号と氏名を表示 ---*/
void PrintData (Data x) {
  printf("番号：%d  氏名：%s\n", x.no, x.name);
}

void PrintTree(BinNode *p) {
  if (p != NULL) {
    PrintTree(p->left);
    PrintData(p->data);
    PrintTree(p->right);
  }
}

/*--- 全ノードを開放 ---*/
void FreeTree (BinNode *p) {
  if (p != NULL){
    FreeTree(p->left);
    FreeTree(p->right);
    free(p);
  }
}

Menu SelectMenu() {
  int ch;

  do {
    printf("(1) 挿入 (2) 探索 (3) 表示 (4) 削除 (0) 終了：");
    scanf("%d", &ch);
  } while (ch < Term || ch > Delete);

  return (Menu)ch;
}

Data Read(char *message, int sw) {
  Data temp;

  printf("%sするデータを入力してください\n", message);

  if (sw & NO) { printf("番号：");  scanf("%d", &temp.no); }
  if (sw & NAME) { printf("名前：");  scanf("%s", temp.name); }

  return temp;
}

/**
 * (1) 挿入 (2) 探索 (3) 表示 (4) 削除 (0) 終了：3
 * 【一覧表】
 * 番号：1  氏名：ann
 * 番号：4  氏名：becky
 * 番号：6  氏名：candy
 * 番号：5  氏名：dan
 * 番号：2  氏名：fred
 * 番号：3  氏名：george
 * (1) 挿入 (2) 探索 (3) 表示 (4) 削除 (0) 終了：4
 * 削除するデータを入力してください
 * 名前：fred
 * 削除しました。
 * (1) 挿入 (2) 探索 (3) 表示 (4) 削除 (0) 終了：3
 * 【一覧表】
 * 番号：1  氏名：ann
 * 番号：4  氏名：becky
 * 番号：6  氏名：candy
 * 番号：5  氏名：dan
 * 番号：3  氏名：george
 */
