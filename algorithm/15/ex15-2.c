/**
 * 課題15-2
 * 提出日 2015/08/04
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: txtSearch関数の中でprintfをしたくなかったけど思いつかなかった.上手く欲しい値を返せなかった
 */

#include <stdio.h>
#include <string.h>

#define MAX_STRLEN 80

int bm_match(char *txt, char *key);
char *txtSearch(char *txt, char *key, int *skip);

void table(char *key, int *skip);

int main() {
  char txt[MAX_STRLEN];
  char key[MAX_STRLEN];

  printf("テキスト: ");
  scanf("%s", txt);

  printf("パターン: ");
  scanf("%s", key);

  int pos = bm_match(txt, key);
  if (pos == -1) {
    printf("見つかりませんでした\n");
  }

  return 0;
}

int bm_match(char *txt, char *key) {
  int skip[256];
  char *p;

  // 飛ばしテーブルの作成
  table(key, skip);
  p = txtSearch(txt, key, skip);

  if (p != NULL) {
    return (int)(p - txt + 1);
  } else {
    return -1;
  }
}

void table(char *key, int *skip) {
  int keySize = strlen(key);

  // initialize
  for (int i = 0; i < 255; i++) {
    skip[i] = keySize;
  }

  // unsigned charでキャストする理由
  // http://stackoverflow.com/questions/9972359/warning-array-subscript-has-type-char
  for (int i = 0; i < keySize - 1; i++) {
    skip[(unsigned char)key[i]] = keySize - 1 - i;
  }
}

char *txtSearch(char *txt, char *key, int *skip) {
  int txtlen = strlen(txt);
  int keylen = strlen(key);
  int pkey;

  int count = 0;
  // beginning check position
  char *p = txt + keylen - 1;

  while (p < txt + txtlen) {
    pkey = keylen - 1;

    while (*p == key[pkey]) {
      // ここのpが出現位置
      if (pkey == 0) {
        printf("%d文字目, %d回目\n", (int)(p - txt + 1), count + 1);
        count++;
      }

      p--;
      pkey--;
    }

    // move check position
    p += (skip[(unsigned char)*p] > keylen - pkey) ?
      skip[(unsigned char)*p] : keylen - pkey;
  }

  if (count == 0) {
    return NULL;
  } else {
    return p;
  }
}
/**
 * テキスト: theuniversityofelectrocommunications
 * パターン: ni
 * 5文字目, 1回目
 * 28文字目, 2回目
 */
