#include <stdio.h>
#include <string.h>

int p_match(char *txt, char *pattern) {
  int ptrtxt = 0;  //テキストの文字列を走査するインデックス
  int ptrpat = 0;  //パターンの文字列を走査するインデックス
  int num = 0;  //一致回数を数えるカウンタ

  while (txt[ptrtxt] != '\0') {
    while (pattern[ptrpat] != '\0' && txt[ptrtxt] == pattern[ptrpat]) {
      ptrtxt++;
      ptrpat++;
    }

    if (pattern[ptrpat] == '\0') num++;

    ptrtxt = ptrtxt - ptrpat + 1;  //照合開始位置を1つずらす
    ptrpat = 0;
  }

  return num;
}

int main(void) {
  char txt[80];
  char keypattern[80];

  printf("テキスト：");
  scanf("%s", txt);

  printf("パターン：");
  scanf("%s", keypattern);

  printf("%d個に見つかりました \n",
         p_match(txt, keypattern));  //文字列（パターン）の探索

  return 0;
}
