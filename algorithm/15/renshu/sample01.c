#include <stdio.h>
#include <string.h>

// boolean
#define TRUE 1
#define FALSE 0;

// utilities
#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

int main() {
  char cities[][10] = {"osaka",  "kyoto",   "nagoya",   "tokyo",
                       "sendai", "sapporo", "hiroshima"};

  int size = SIZE_OF_ARRAY(cities);
  int flag = FALSE;

  for (int i = 0; i < size; i++) {
    if (strchr(cities[i], 'o')) {
      flag = TRUE;
    } else {
      flag = FALSE;
    }

    printf("cities[%d] = %s length = %d flag = %d\n", i, cities[i],
           (int)strlen(cities[i]), flag);
  }

  return 0;
}
