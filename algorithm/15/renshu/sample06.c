#include <stdio.h>
#include <string.h>

#define MAX_STRLEN 80

int bm_match(char *txt, char *key);
char *txtSearch(char *txt, char *key, int *skip);

void table(char *key, int *skip);

int main() {
  char txt[MAX_STRLEN];
  char key[MAX_STRLEN];

  printf("テキスト: ");
  scanf("%s", txt);

  printf("パターン: ");
  scanf("%s", key);

  int pos = bm_match(txt, key);
  if (pos != -1) {
    printf("%d文字目に見つかりました\n", pos);
  } else {
    printf("見つかりませんでした\n");
  }

  return 0;
}

int bm_match(char *txt, char *key) {
  int skip[256];
  char *p;

  // 飛ばしテーブルの作成
  table(key, skip);
  p = txtSearch(txt, key, skip);

  if (p != NULL) {
    return (int)(p - txt + 1);
  } else {
    return -1;
  }
}

char *txtSearch(char *txt, char *key, int *skip) {
  int txtlen = strlen(txt);
  int keylen = strlen(key);
  int pkey;

  char *p;

  // beginning check position
  p = txt + keylen - 1;

  while (p < txt + txtlen) {
    pkey = keylen - 1;

    while (*p == key[pkey]) {
      // ここのpが出現位置
      if (pkey == 0) return p;

      p--;
      pkey--;
    }

    // move check position
    p += (skip[(unsigned char)*p] > keylen - pkey) ?
      skip[(unsigned char)*p] : keylen - pkey;
  }

  return NULL;
}

void table(char *key, int *skip) {
  int keySize = strlen(key);

  // initialize
  for (int i = 0; i < 255; i++) {
    skip[i] = keySize;
  }

  // unsigned charでキャストする理由
  // http://stackoverflow.com/questions/9972359/warning-array-subscript-has-type-char
  for (int i = 0; i < keySize - 1; i++) {
    skip[(unsigned char)key[i]] = keySize - 1 - i;
  }
}
