#include <stdio.h>
#include <string.h>

#define MAX_LEN 10

// utilities
#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void strSelectionSort(char str[][MAX_LEN], int size);

int main() {
  char cities[][10] = {"osaka",  "kyoto",   "nagoya",   "tokyo",
                       "sendai", "sapporo", "hiroshima"};

  int size = SIZE_OF_ARRAY(cities);

  for (int i = 0; i < size; i++) {
    printf("before sort cities[%d] = %s\n", i, cities[i]);
  }

  strSelectionSort(cities, size);

  for (int i = 0; i < size; i++) {
    printf("sorted cities[%d] = %s\n", i, cities[i]);
  }

  return 0;
}

void strSelectionSort(char str[][MAX_LEN], int size) {
  char *min;
  char tmp[MAX_LEN];

  for (int i = 0; i < size - 1; i++) {
    strcpy(tmp, (char *)str[i]);
    min = str[i];

    for (int j = i + 1; j < size; j++) {
      if (strcmp(str[i], str[j]) > 0) {
        strcpy(str[i], str[j]);
        min = str[j];
      }
    }
    strcpy(min, tmp);
  }
}
