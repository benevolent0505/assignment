#include <stdio.h>
#include <string.h>

int p_match(char *txt, char *pattern);

int main(void) {
  char txt[80];
  char keypattern[80];

  printf("テキスト：");
  scanf("%s", txt);
  printf("パターン：");
  scanf("%s", keypattern);

  int pos = p_match(txt, keypattern);
  if (pos != -1) {
    printf("%d文字目に見つかりました\n", pos + 1);
  } else {
    printf("見つかりませんでした\n");
  }

  return 0;
}

int p_match(char *txt, char *pattern) {
  int ptrtxt = 0;  //テキストの文字列を走査するインデックス
  int ptrpat = 0;  //パターンの文字列を走査するインデックス

  while (txt[ptrtxt] != '\0' && pattern[ptrpat] != '\0') {
    if (txt[ptrtxt] == pattern[ptrpat]) {
      ptrtxt++;
      ptrpat++;
    } else {                         //文字照合が失敗した場合
      ptrtxt = ptrtxt - ptrpat + 1;  //照合開始位置を1つずらす
      ptrpat = 0;
    }
  }

  if (pattern[ptrpat] == '\0') {
    return (ptrtxt - ptrpat);  //文字列が一致した先頭位置を返す
  }

  return -1;
}
