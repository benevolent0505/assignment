#include <stdio.h>
#include <string.h>

#define MAX_LEN 10

// utilities
#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void strSelectionSort(char *str, int size);

int main() {
  char cities[][MAX_LEN] = {"osaka",  "kyoto",   "nagoya",   "tokyo",
                            "sendai", "sapporo", "hiroshima"};

  int size = SIZE_OF_ARRAY(cities);

  for (int i = 0; i < size; i++) {
    printf("before sort cities[%d] = %s\n", i, cities[i]);
  }

  strSelectionSort((char *)cities, size);

  /* for (int i = 0; i < size; i++) { */
  /*   printf("sorted cities[%d] = %s\n", i, cities[i]); */
  /* } */

  return 0;
}

void strSelectionSort(char *str, int size) {
  for (int i = 0; i < size - 1; i++) {
    printf("%s\n", str);
    str;
  }
}
