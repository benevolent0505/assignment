#include <stdio.h>
#include <string.h>

void table(char *key, int *skip);

int main() {
  int i;
  char txt[80];
  char key[80];

  printf("パターン：");
  scanf("%s", key);

  int keylen = strlen(key);
  int skip[256];

  table(key, skip);

  for (i = 0; i < keylen; i++) {
    printf("skip[%c]=%d \n", key[i], skip[key[i]]);
  }

  return 0;
}

void table(char *key, int *skip) {
  int n, k;

  n = strlen(key);
  for (k = 0; k < 255; k++) {
    skip[k] = n;
  }

  for (k = 0; k < n - 1; k++) {
    skip[key[k]] = n - 1 - k;
  }
}
