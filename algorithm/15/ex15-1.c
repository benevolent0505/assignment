/**
 * 課題15-1
 * 提出日 2015/08/04
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: クイックソートの回のコードをほとんどコピペするだけで出来た
 */

#include <stdio.h>
#include <string.h>

#define MAX_LEN 10

#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void strQuickSort(char str[][MAX_LEN], int left, int right);

char *selectPivot(char *left, char *middle, char *right);
void strSwap(char *a, char *b);

int main() {
  char cities[][MAX_LEN] = {
    "osaka", "kyoto", "nagoya", "tokyo", "sendai", "sapporo", "hiroshima"
  };

  int size = SIZE_OF_ARRAY(cities);

  for (int i = 0; i < size; i++) {
    printf("before sort cities[%d] = %s\n", i, cities[i]);
  }

  strQuickSort(cities, 0, size - 1);

  for (int i = 0; i < size; i++) {
    printf("sorted cities[%d] = %s\n", i, cities[i]);
  }

  return 0;
}

void strQuickSort(char str[][MAX_LEN], int left, int right) {
  int middle = (left + right) / 2;
  int pl, pr;
  char *pivot;

  if (left < right) {
    pivot = selectPivot(str[left], str[middle], str[right]);

    pl = left - 1;
    pr = right + 1;

    do {
      pl++;
      pr--;

      while (strcmp(str[pl], pivot) < 0) pl++;
      while (strcmp(str[pr], pivot) > 0) pr--;

      if (pl < pr) strSwap(str[pl], str[pr]);
    } while (pl < pr);

    strQuickSort(str, left, pl - 1);
    strQuickSort(str, pr + 1, right);
  }
}

char *selectPivot(char *left, char *middle, char *right) {
  char *pivot;

  if (strcmp(left, middle) < 0) {
    if (strcmp(right, left) < 0) {
      pivot = left;
    } else if (strcmp(middle, right) < 0) {
      pivot = middle;
    } else {
      pivot = right;
    }
  } else if (strcmp(left, middle) > 0) {
    if (strcmp(left, right) < 0) {
      pivot = left;
    } else if (strcmp(right, middle) < 0) {
      pivot = middle;
    } else {
      pivot = right;
    }
  } else {
    pivot = middle;
  }

  return pivot;
}

void strSwap(char *a, char *b) {
  int cnt = 0;

  while (a[cnt] || b[cnt]) {
    char tmp = a[cnt];

    a[cnt] = b[cnt];
    b[cnt] = tmp;

    cnt++;
  }
}
/**
 * before sort cities[0] = osaka
 * before sort cities[1] = kyoto
 * before sort cities[2] = nagoya
 * before sort cities[3] = tokyo
 * before sort cities[4] = sendai
 * before sort cities[5] = sapporo
 * before sort cities[6] = hiroshima
 * sorted cities[0] = hiroshima
 * sorted cities[1] = kyoto
 * sorted cities[2] = nagoya
 * sorted cities[3] = osaka
 * sorted cities[4] = sapporo
 * sorted cities[5] = sendai
 * sorted cities[6] = tokyo
 */
