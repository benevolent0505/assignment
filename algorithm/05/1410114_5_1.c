/**
 * 課題5-1
 * 提出日 2015/05/26
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: 値渡しと参照渡しについて意識できた気がする。
 */

#include <stdio.h>
#include <stdlib.h>

#define N 10

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

typedef struct __Stack {
  Node *top;
  int nitems;
} Stack;

void Init(Stack *s);
void StackDump(Stack *s);

void Push(Stack *s, int x);
int Pop(Stack *s);

int main() {
  Stack stk;

  Init(&stk);

  for (int i = 0; i < N; i++) {
    // Push operation
    Push(&stk, i);
  }

  printf("check the stack dump\n");
  StackDump(&stk);

  printf("pop operation\n");
  for (int i = 0; i < N; i++) {
    printf("%d\n", Pop(&stk));
  }

  printf("%d\n", Pop(&stk));

  Push(&stk, 100);
  printf("%d\n", Pop(&stk));

  return 0;
}

void Init(Stack *s) {
  s->nitems = 0;
  s->top = NULL;
}

void StackDump(Stack *s) {
  Node *p;
  p = s->top;
  for (p = s->top; p != NULL; p = p->next) {
    printf("%d\n", p->data);
  }
}

void Push(Stack *s, int x) {
  Node *p;

  p = s->top;
  s->top = malloc(sizeof(Node));
  s->top->data = x;
  s->top->next = p;
  s->nitems++;
}

int Pop(Stack *s) {
  Node *p;
  int popval = -1;

  if (s->top == NULL) return popval;

  p = s->top->next;
  popval = s->top->data;

  free(s->top);

  s->top = p;
  s->nitems--;

  return popval;
}

/**
 * check the stack dump
 * 9
 * 8
 * 7
 * 6
 * 5
 * 4
 * 3
 * 2
 * 1
 * 0
 * pop operation
 * 9
 * 8
 * 7
 * 6
 * 5
 * 4
 * 3
 * 2
 * 1
 * 0
 * -1
 * 100
 */
