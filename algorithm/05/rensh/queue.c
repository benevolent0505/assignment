#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

typedef struct __Queue {
  Node *top;
  Node *bottom;
  int nitems;
} Queue;

#define N 10

void init(Queue *q);
void dumpQueue(Queue *q);

void enqueue(int data, Queue *que);
int dequeue(Queue *que);

int main() {
  Queue que;

  init(&que);

  for (int i = 0; i < N; i++) {
    enqueue(i, &que);
  }

  printf("check the queue dump\n");
  dumpQueue(&que);

  printf("dequeue operations\n");

  for (int i = 0; i < N; i++) {
    printf("%d\n", dequeue(&que));
  }

  return 0;
}

void init(Queue *que) {
  que->nitems = 0;
  que->top = NULL;
  que->bottom = NULL;
}

void dumpQueue(Queue *que) {
  Node *p;

  for (p = que->top; p != NULL; p = p->next) {
    printf("%d\n", p->data);
  }
}

void enqueue(int data, Queue *que) {
  Node *p = malloc(sizeof(Node));

  p->data = data;
  p->next = NULL;

  if (que->bottom != NULL) {
    que->bottom->next = p;
  } else {
    que->top = p;
  }

  que->bottom = p;
  que->nitems++;
}

int dequeue(Queue *que) {
  if (que->top == NULL) return -1;

  int queval = -1;
  Node *p;

  if (que->top != NULL) {
    queval = que->top->data;
    p = que->top->next;

    free(que->top);

    que->top = p;

    if (p == NULL) {
      que->bottom = NULL;
    }
  }
  que->nitems--;

  return queval;
}
