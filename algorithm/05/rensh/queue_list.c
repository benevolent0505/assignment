#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

typedef struct __Queue {
  Node *top;
  Node *bottom;
  int nitems;
} Queue;

#define N 10

void Init(Queue *q);
void QueueDump(Queue *q);

int main() {
  int i;
  Queue que;

  Init(&que);

  for (i = 0; i < N; i++) {
    // Enqueue operation
    Node *p;

    p = malloc(sizeof(Node));
    p->data = i;
    p->next = NULL;
    if (que.bottom != NULL) {
      que.bottom->next = p;
    } else {  // Queue is empty
      que.top = p;
    }
    que.bottom = p;
    que.nitems++;
  }

  printf("check the queue dump\n");
  QueueDump(&que);

  printf("dequeue operations\n");
  for (i = 0; i < N; i++) {
    int queval = -1;
    // Dequeue operation
    Node *p;  // save for top->next

    if (que.top != NULL) {
      queval = que.top->data;
      p = que.top->next;
      free(que.top);
      que.top = p;
      if (p == NULL) {
        que.bottom = NULL;
      }
    }
    que.nitems--;

    printf("%d\n", queval);
  }

  return 0;
}

void Init(Queue *q) {
  q->nitems = 0;
  q->top = NULL;
  q->bottom = NULL;
}

void QueueDump(Queue *q) {
  Node *p;
  for (p = q->top; p != NULL; p = p->next) {
    printf("%d\n", p->data);
  }
}
