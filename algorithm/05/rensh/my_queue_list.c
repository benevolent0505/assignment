#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

typedef struct __Queue {
  Node *top;
  Node *bottom;
  int size;
} Queue;

#define N 10

void init(Queue *que);
void queueDump(Queue *que);

void enqueue(Queue *que, int x);
int dequeue(Queue *que);

int main() {
  Queue que;

  init(&que);

  for (int i = 0; i < N; i++) {
    enqueue(&que, i);
  }

  printf("check the queue dump\n");
  queueDump(&que);

  printf("dequeue operations\n");
  for (int i = 0; i < N; i++) {
    printf("%d\n", dequeue(&que));
  }

  return 0;
}

void init(Queue *que) {
  que->size = 0;
  que->top = NULL;
  que->bottom = NULL;
}

void queueDump(Queue *que) {
  Node *p;

  for (p = que->top; p != NULL; p = p->next) {
    printf("%d\n", p->data);
  }
}

void enqueue(Queue *que, int x) {
  Node *p = malloc(sizeof(Node));

  p->data = x;
  p->next = NULL;
  if (que->bottom != NULL) {
    que->bottom->next = p;
  } else {
    que->top = p;
  }
  que->bottom = p;
  que->size++;
}

int dequeue(Queue *que) {
  int dequeval = -1;

  if (que->top != NULL) {
    dequeval = que->top->data;
    Node *p = que->top->next;

    free(que->top);

    que->top = p;

    if (p == NULL) {
      que->bottom = NULL;
    }

    que->size--;
  }

  return dequeval;
}
