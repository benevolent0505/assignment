/* 配列を用いてキューにおいて， Dequeue 操作は，要素のコピーを行う為，
 * 効率が悪いものになってしまいがちです．これを改善するような方法を考えて，
 * 関数として実現しなさい．  */
/* 例としては，リングバッファ（参考書 p.112） のような構造を用いることで，
 * 常にキューの先頭と最後尾を保存しておくことによって計算量のオーダーを下げることができます．
 */

#include <stdio.h>

#define N 10

typedef struct __Queue {
  int array[N];
  int size;  // Current data size
} Queue;

void init(Queue *que);
void queueDump(Queue *que);

void enqueue(Queue *que, int x);
int dequeue(Queue *que);

int main() {
  Queue queue;

  init(&queue);
  queueDump(&queue);

  for (int i = 0; i < 10; i++) {
    enqueue(&queue, i);
  }

  queueDump(&queue);

  for (int i = 0; i < 10; i++) {
    printf("%d\n", dequeue(&queue));
  }

  queueDump(&queue);

  return 0;
}

void init(Queue *que) { que->size = 0; }
void queueDump(Queue *que) {
  for (int i = 0; i < que->size; i++) {
    printf("array[%d]: %d\n", i, que->array[i]);
  }
}

void enqueue(Queue *que, int x) {
  if (que->size < N) {
    que->array[que->size] = x;
    que->size++;
  }
}

// not clever code
int dequeue(Queue *que) {
  int dequeval = -1;

  if (que->size > 0) {
    dequeval = que->array[0];

    // order n
    for (int i = 1; i < que->size; i++) {
      que->array[i - 1] = que->array[i];
    }
    que->size--;
  }

  return dequeval;
}
