#include <stdio.h>
#include <stdlib.h>

typedef struct __Node {
  int data;
  struct __Node *next;
} Node;

typedef struct __Stack {
  Node *top;
  int nitems;
} Stack;

void Init(Stack *s);
void StackDump(Stack *s);

#define N 10

int main() {
  int i;
  Stack stk;

  Init(&stk);

  for (i = 0; i < N; i++) {
    Node *p;

    // Push operation
    p = stk.top;  // save for top->next
    stk.top = malloc(sizeof(Node));
    stk.top->data = i;
    stk.top->next = p;
    stk.nitems++;
  }

  printf("check the stack dump\n");
  StackDump(&stk);

  printf("pop operation\n");
  for (i = 0; i < N; i++) {
    int popval = -1;
    Node *p;

    // Pop operation
    p = stk.top->next;  // save for top
    popval = stk.top->data;
    free(stk.top);
    stk.top = p;
    stk.nitems--;

    printf("%d\n", popval);
  }

  return 0;
}

void Init(Stack *s) {
  s->nitems = 0;
  s->top = NULL;
}

void StackDump(Stack *s) {
  Node *p;
  p = s->top;
  for (p = s->top; p != NULL; p = p->next) {
    printf("%d\n", p->data);
  }
}
