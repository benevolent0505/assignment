#include <stdio.h>

#define N 10

typedef struct __Queue {
  int ary[N];
  int nitems;  // Current data size
} Queue;

void Init(Queue *q);
void QueueDump(Queue *q);

int main() {
  Queue que;

  Init(&que);

  for (int i = 0; i < N; i++) {
    // Enqueue operation
    if (que.nitems < N) {
      que.ary[que.nitems] = i;
      que.nitems++;
    }
  }

  QueueDump(&que);

  for (int i = 0; i < 10; i++) {
    int queval = -1;

    // Dequeue operation
    if (que.nitems > 0) {
      queval = que.ary[0];
      for (int j = 1; j < que.nitems; j++) {  // not clever code
        que.ary[j - 1] = que.ary[j];
      }
      que.nitems--;
    }

    printf("%d\n", queval);
  }

  return 0;
}

void Init(Queue *q) { q->nitems = 0; }

void QueueDump(Queue *q) {
  for (int i = 0; i < q->nitems; i++) {
    printf("ary[%d]: %d\n", i, q->ary[i]);
  }
}
