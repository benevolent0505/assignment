#include <stdio.h>

#define N 10

typedef struct __Stack {
  int ary[N];
  int nitems;  // Current data size
} Stack;

void Init(Stack *s);
void StackDump(Stack *s);

int main(void) {
  Stack stk;

  Init(&stk);

  for (int i = 0; i < N; i++) {
    // Push operation
    if (stk.nitems < N) {
      stk.ary[stk.nitems] = i;
      stk.nitems++;
    }
  }

  printf("check the stack dump\n");
  StackDump(&stk);

  printf("\npop operation\n");
  for (int i = 0; i < N; i++) {
    int popval = -1;

    // Pop operation
    if (stk.nitems > 0) {
      popval = stk.ary[stk.nitems - 1];
      stk.nitems--;
    }

    printf("%d\n", popval);
  }

  return 0;
}

void Init(Stack *s) { s->nitems = 0; }

void StackDump(Stack *s) {
  for (int i = s->nitems - 1; i >= 0; i--) {
    printf("ary[%d]: %d\n", i, s->ary[i]);
  }
}
