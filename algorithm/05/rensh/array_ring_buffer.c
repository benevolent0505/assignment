#include <stdio.h>

#define N 10

typedef struct __Queue {
  int array[N];
  int i_size;  // Current data size
  int r_size;
  int top_index;
} Queue;

void init(Queue *que);
void queueDump(Queue *que);

void enqueue(Queue *que, int x);
int dequeue(Queue *que);

int main() {
  Queue queue;

  init(&queue);
  queueDump(&queue);

  for (int i = 0; i < 10; i++) {
    enqueue(&queue, i);
  }

  queueDump(&queue);

  for (int i = 0; i < 10; i++) {
    printf("%d\n", dequeue(&queue));
  }

  queueDump(&queue);

  return 0;
}

void init(Queue *que) {
  que->i_size = 0;
  que->r_size = 0;
  que->top_index = 0;
}
void queueDump(Queue *que) {
  for (int i = 0; i < que->i_size; i++) {
    printf("array[%d]: %d\n", i, que->array[i]);
  }
}

void enqueue(Queue *que, int x) {}

int dequeue(Queue *que) {}
