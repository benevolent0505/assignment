/*
  課題2-1
  提出日 2015/04/22
  学籍番号 1410114
  氏名 藤田 幹央
  感想: C言語の変数名のいい感じの省略が思いつかない。
 */

#include <stdio.h>

#define N 200

int miniarray(int arr[], int n);

int main() {
  int i;
  int a[N];
  int min;

  for (i = 0; i < N; i++) {
    scanf("%d\n", &a[i]);
  }

  min = miniarray(&a[20], 100);
  printf("%d\n", min);

  return 0;
}

int miniarray(int arr[], int n) {
  int i;
  int min;

  min = arr[0];

  for (i = 0; i < n; i++) {
    if (min > arr[i]) {
      min = arr[i];
    }
  }

  return min;
}
/*
  > 40
 */
