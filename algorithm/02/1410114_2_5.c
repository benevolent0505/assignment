/*
  課題2-5
  提出日 2015/04/29
  学籍番号 1410114
  氏名 藤田 幹央
  感想: 点が交差しないように、（反）時計周りに点を与えていけば、周囲長が求まる.
       2-4よりは見通しがましになった気がします.
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

typedef struct Point2D {
  double x;
  double y;
} Point2D;

typedef struct Polygon {
  int n;
  Point2D *point;

  double perimeter;
} Polygon;

Point2D ScanPoint2D();

double GetPerimeter(Polygon p);
double GetDistance(Point2D p1, Point2D p2);

int main() {
  int i;
  Polygon p;

  printf("Input the number of polygon's point:");
  scanf("%d", &p.n);
  p.point = malloc(sizeof(Point2D) * p.n);

  for (i = 0; i < p.n; i++) {
    p.point[i] = ScanPoint2D();
  }

  p.perimeter = GetPerimeter(p);
  printf("This polygon's perimeter is %lf.\n", p.perimeter);

  free(p.point);

  return 0;
}

Point2D ScanPoint2D() {
  Point2D p;

  printf("Input 2 values(x and y):\n");
  scanf("%lf", &p.x);
  scanf("%lf", &p.y);

  return p;
}

double GetPerimeter(Polygon p) {
  int i;

  double perimeter;

  perimeter = 0.0;
  for (i = 0; i < p.n; i++) {
    if (i + 1 < p.n) {
      perimeter += GetDistance(p.point[i], p.point[i + 1]);
    } else {
      perimeter += GetDistance(p.point[i], p.point[0]);
    }
  }

  return perimeter;
}

double GetDistance(Point2D p1, Point2D p2) {
  double distance;

  distance = sqrt(pow((p1.x - p2.x), 2.0) + pow((p1.y - p2.y), 2.0));

  return distance;
}
/*
  5 角形で(0, 0), (3, -1), (4, 1), (2, 4), (0, 1)のテスト結果
  > This polygon's perimeter is 13.609448.
 */
