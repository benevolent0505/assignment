/*
  課題2-4
  提出日 2015/04/29
  学籍番号 1410114
  氏名 藤田 幹央
  感想: 入力の1点目と3点目を結んだ線分で、対角線が引けることが入力の条件になる.
        まだ煩雑な部分（SquareとTriangleの２つがあることなど）があるので、整えることが出来そうな気がする。
 */

#include <stdio.h>
#include <math.h>

typedef struct Point2D {
  double x;
  double y;
} Point2D;

typedef struct Square {
  Point2D p[4];

  double perimeter;
  double area;
} Square;

typedef struct Triangle { Point2D p[3]; } Triangle;

Point2D ScanPoint2D();

double GetPerimeter(Square s);
double GetArea(Square s);

double GetDistance(Point2D p1, Point2D p2);
double GetTriangleArea(Triangle t);

int main() {
  int i;
  Square s;

  for (i = 0; i < 4; i++) {
    s.p[i] = ScanPoint2D();
  }

  s.perimeter = GetPerimeter(s);
  s.area = GetArea(s);

  printf("Square's perimeter is %lf.\nSquare's area is %lf.\n", s.perimeter,
         s.area);

  return 0;
}

Point2D ScanPoint2D() {
  Point2D p;

  printf("Input 2 values(x and y):\n");
  scanf("%lf", &p.x);
  scanf("%lf", &p.y);

  return p;
}

double GetPerimeter(Square s) {
  double perimeter;

  perimeter = GetDistance(s.p[0], s.p[1]) + GetDistance(s.p[1], s.p[2]) +
              GetDistance(s.p[2], s.p[3]) + GetDistance(s.p[3], s.p[0]);

  return perimeter;
}

double GetArea(Square s) {
  double area;
  Triangle t1, t2;

  t1.p[0] = s.p[0], t1.p[1] = s.p[1], t1.p[2] = s.p[2];
  t2.p[0] = s.p[0], t2.p[1] = s.p[2], t2.p[2] = s.p[3];

  area = GetTriangleArea(t1) + GetTriangleArea(t2);

  return area;
};

double GetDistance(Point2D p1, Point2D p2) {
  double distance;

  distance = sqrt(pow((p1.x - p2.x), 2.0) + pow((p1.y - p2.y), 2.0));

  return distance;
}

double GetTriangleArea(Triangle t) {
  double area;
  double s, a, b, c;

  a = GetDistance(t.p[0], t.p[1]);
  b = GetDistance(t.p[0], t.p[2]);
  c = GetDistance(t.p[1], t.p[2]);
  s = (a + b + c) / 2;

  area = sqrt(s * (s - a) * (s - b) * (s - c));

  return area;
}

/*
  (10, 0), (20, 10), (10, 20), (0, 10)のテスト結果
  > Square's perimeter is 56.568542.
  > Square's area is 200.000000.
 */
