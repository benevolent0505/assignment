/*
  課題 2-2
  提出日 2015/04/22
  学籍番号 1410114
  氏名 藤田 幹央
  感想: mallocを初めて使いました。
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
  int i;
  int *p;
  int n;
  double ave;

  // 格納したいメモリ数
  scanf("%d\n", &n);

  // メモリを確保
  p = malloc(sizeof(int) * n);

  // データを格納
  for (i = 0; i < n; i++) {
    scanf("%d", &p[i]);
  }

  // 初期化
  ave = 0;

  // 平均値の計算
  for (i = 0; i < n; i++) {
    ave += p[i];
  }

  printf("%6.4f\n", ave / n);

  free(p);

  return 0;
}
/*
  > 59.8000
 */
