/*
  課題2-3
  提出日 2015/04/22
  学籍番号 1410114
  氏名 藤田 幹央
  感想: 冗長な書き方をしてしまったのでつらいです。
 */

#include <stdio.h>
#include <math.h>

typedef struct _Point2D {
  double x;
  double y;
} Point2D;

typedef struct Triangle {
  Point2D p1;
  Point2D p2;
  Point2D p3;
} Triangle;

double GetAreaByHeron(Triangle t);
double GetDistance2D(Point2D a, Point2D b);

int main() {
  Point2D a, b, c;
  Triangle t;
  double area;

  a.x = 0.0;
  a.y = 1.0;

  b.x = 1.0;
  b.y = 0.0;

  c.x = 0.0;
  c.y = 0.0;

  t.p1 = a;
  t.p2 = b;
  t.p3 = c;

  area = GetAreaByHeron(t);

  printf("%5.2f\n", area);

  return 0;
}

double GetAreaByHeron(Triangle t) {
  double area;
  double s;
  double a, b, c;

  a = GetDistance2D(t.p1, t.p2);
  b = GetDistance2D(t.p1, t.p3);
  c = GetDistance2D(t.p2, t.p3);
  s = (a + b + c) / 2;
  area = sqrt(s * (s - a) * (s - b) * (s - c));

  return area;
}

double GetDistance2D(Point2D a, Point2D b) {
  double d;

  d = sqrt(pow((a.x - b.x), 2.0) + pow((a.y - b.y), 2.0));

  return d;
}

/*
  > 0.50
 */
