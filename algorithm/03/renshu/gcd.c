#include <stdio.h>

int getGCD(int a, int b);

int main() {
  int a, b, gcd;

  printf("Input 2 values:\n");
  scanf("%d", &a);
  scanf("%d", &b);

  gcd = getGCD(a, b);

  if (gcd == 0) {
     printf("No Greatest Common Divisor\n");
  } else {
    printf("Greatest Common Divisor is %d\n", gcd);
  }

  return 0;
}

int getGCD(int a, int b) {
  int i;

  int gcd;
  int max;

  max = a < b ? b : a;

  gcd = 0;
  for (i = 1; i <= max; i++) {
    if (a % i == 0 && b % i == 0) {
      gcd = i;
    }
  }

  return gcd;
}
