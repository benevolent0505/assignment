#include <stdio.h>

int getGCD(int a, int b);
void swap(int a, int b);

int main() {
  int a, b, gcd;

  printf("Input 2 values:\n");
  scanf("%d", &a);
  scanf("%d", &b);

  gcd = getGCD(a, b);

  printf("Greatest Common Divisor is %d\n", gcd);

  return 0;
}

int getGCD(int a, int b) {
  int r;

  if (a < b) {
    swap(a, b);
  }

  while (b > 0) {
    r = a % b;
    a = b;
    b = r;
  }

  return a;
}

void swap(int a, int b) {
  int tmp;

  tmp = a;
  a = b;
  b = tmp;
}
