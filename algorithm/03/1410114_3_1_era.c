#include <stdio.h>
#include <math.h>

void eratosthenes(int n);
void nonEratosthenes(int n);

int main() {
  int n = 1000000;

  nonEratosthenes(n);

  return 0;
}

void eratosthenes(int n) {
  int i = 0;
  int j = 0;

  int find[n];

  find[0] = 0;
  find[1] = 0;

  for (i = 2; i < n; i++) {
    find[i] = i;
  }

  for (i = 2; i <= (int)sqrt(n); i++) {
    if (find[i] != 0) {
      for (j = 2; j < n; j++) {
        if (i * j > n) break;
        find[i * j] = 0;
      }
    }
  }

  for (i = 2; i <= n; i++) {
    if (find[i] != 0) {
      printf("%d\n", find[i]);
    }
  }
}

void nonEratosthenes(int n) {
  int i, j;

  for (i = 2; i <= n; i++) {
    for (j = 2; j <= n; j++) {
      if (i < j) break;

      if (i % j == 0) {
        if (i != j) break;
        printf("%d\n", i);
      }
    }
  }
}
