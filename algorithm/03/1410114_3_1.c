/*
  課題3-1
  提出日 2015/05/13
  学籍番号 1410114
  氏名 藤田 幹央
  感想:
  実行時間に明らかな違いが出たので、アルゴリズムを勉強する必要性をとても感じた.
 */

#include <stdio.h>
#include <math.h>

void eratosthenes(int n);
void nonEratosthenes(int n);

int main() {
  int n = 1000000;

  eratosthenes(n);

  return 0;
}

void eratosthenes(int n) {
  int i = 0;
  int j = 0;

  int find[n];

  find[0] = 0;
  find[1] = 0;

  for (i = 2; i < n; i++) {
    find[i] = i;
  }

  for (i = 2; i <= (int)sqrt(n); i++) {
    if (find[i] != 0) {
      for (j = 2; j < n; j++) {
        if (i * j > n) break;
        find[i * j] = 0;
      }
    }
  }

  for (i = 2; i <= n; i++) {
    if (find[i] != 0) {
      printf("%d\n", find[i]);
    }
  }
}

void nonEratosthenes(int n) {
  int i, j;

  for (i = 2; i <= n; i++) {
    for (j = 2; j <= n; j++) {
      if (i < j) break;

      if (i % j == 0) {
        if (i != j) break;
        printf("%d\n", i);
      }
    }
  }
}

/*
  ``` 実行結果
  2
  3
  5
  7
  ~~
  中略
  ~~
  999961
  999979
  999983
  ```

  100万までの素数をエラトステネスの篩を用いたeratosthenes関数で列挙し実行時間と、用いなかったnonEratosthenes関数の実行時間を以下に示す.

  |-----------------+---------+---------+-------|
  |                 | real    | user    | sys   |
  |-----------------+---------+---------+-------|
  | eratosthenes    | 0.21s   | 0.07s   | 0.07s |
  |-----------------+---------+---------+-------|
  | nonEratosthenes | 197.69s | 196.51s | 1.05s |
  |-----------------+---------+---------+-------|

  以下で計算量について考察する.

  ## nonEratosthenes関数
  計算量はO(n^2)になる

  ## eratosthenes関数
  計算量はO(nlog(logn))になる(素数の逆数和はlog(logn)に近づくので)

  O(logn) <
  O(n)よりエラトステネスの篩を用いたほうが計算量が少ないため、実行時間も短いことがわかる.
 */
