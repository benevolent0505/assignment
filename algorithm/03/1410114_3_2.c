// 模範解答
#include <stdio.h>
#include <stdlib.h>

int ssum(int a[], int n, int b);
int power(int y, int n);
void make_pattern(int x[], int n, int k);

int main(void) {
  int ans1, ans2;

  int a1[] = {3, 7, 5, 8, 2};
  int n1 = 5;
  int b1 = 11;

  int a2[] = {1267, 49300, 869, 961,  300101, 1153, 1246,  1598, 1766, 292210,
              2666, 2349,  121, 1388, 522213, 1342, 10234, 1131, 1234, 230010};
  int n2 = 20;
  int b2 = 1405335;

  ans1 = ssum(a1, n1, b1);
  ans2 = ssum(a2, n2, b2);

  if (ans1 == 1) {
    printf("Find a subset sum for problem 1.\n");
  } else {
    printf("No solution for problem 1 sum.\n");
  }

  if (ans2 == 1) {
    printf("Find a subset sum for problem 2.\n");
  } else {
    printf("No solution for problem 2 sum.\n");
  }
  return 0;
}

int ssum(int a[], int n, int b) {
  int ans = 0;
  int *x;
  int i, xnum;

  x = malloc(sizeof(int) * n);
  xnum = power(2, n);  // total bit patterns

  for (i = 0; i < xnum; i++) {
    int j, ss;
    make_pattern(x, n, i);

    // calculate subset sum
    ss = 0;
    for (j = 0; j < n; j++) {
      ss += a[j] * x[j];
    }
    if (ss == b) {  // find a solution
      ans = 1;
      break;
    }
  }

  // if you'd like to display the solution, comment out followings
  /*
  if( ans == 1 ){
      printf( "find sum for %d:\n", b );
      for( i = 0 ; i < n ; i++ ){
          if( x[i] == 1 ){
              printf( "%d\n", a[i] );
          }
      }
  }
  */

  free(x);

  return ans;
}

void make_pattern(int x[], int n, int k) {
  // make bit pattern for index k
  int i;

  // if you'd like to see the pattern, comment out printf() sentences.
  for (i = 0; i < n; i++) {
    x[i] = k % 2;
    k = k / 2;
    // printf( "%d", x[i] );
  }
  // printf( "\n" );
}

int power(int y, int n) {
  int a = 1;
  int i;

  for (i = 0; i < n; i++) {
    a *= y;
  }
  return a;
}
