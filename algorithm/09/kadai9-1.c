/**
 * 課題9-1
 * 提出日 2015/06/21
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: 再帰処理にまだ全然慣れない.
 */

#include <stdio.h>

int fibo(int n);

int main() {
  int n = 0;

  printf("フィボナッチ数列の長さ: ");
  scanf("%d", &n);

  for (int i = 0; i < n; i++) {
    printf("%d\n", fibo(i));
  }

  return 0;
}

int fibo(int n) {
  if (n > 1) {
    return fibo(n - 1) + fibo(n - 2);
  } else if (n == 1){
    return 1;
  } else {
    return 0;
  }
}
/**
 * フィボナッチ数列の長さ: 20
 * 0
 * 1
 * 1
 * 2
 * 3
 * 5
 * 8
 * 13
 * 21
 * 34
 * 55
 * 89
 * 144
 * 233
 * 377
 * 610
 * 987
 * 1597
 * 2584
 * 4181
 */
