/**
 * 課題9-2
 * 提出日 2015/06/21
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: continue文を使うアイデア今後も覚えておきたい
 */

#include <stdio.h>

int count;

int pos[8];
int r_flag[8];
int d_flag[13];
int id_flag[13];

void print();
void set(int i);

int main() {
  set(0);

  printf("count = %d\n", count);

  return 0;
}

void print() {
  for (int i = 0; i < 8; i++) {
    printf("%2d", pos[i]);
  }
  putchar('\n');
}

void set(int n) {
  for (int i = 0; i < 8; i++) {
    if (!r_flag[i] && !d_flag[i+n] && !id_flag[i-n+7]) {

      // 課題9-2フィルター！！！！！
      // いい感じになった
      if (n > 0) {
        if (pos[n-1] == i - 2 || pos[n-1] == i + 2) continue;
      }

      pos[n] = i;

      if (n == 7) {
        print();
        count++;
      } else {
        r_flag[i] = 1;
        d_flag[i+n] = 1;
        id_flag[i-n+7] = 1;

        set(n + 1);

        r_flag[i] = 0;
        d_flag[i+n] = 0;
        id_flag[i-n+7] = 0;
      }
    }
  }
}
/**
 *  2 5 1 4 7 0 6 3
 *  2 5 1 6 0 3 7 4
 *  2 7 3 6 0 5 1 4
 *  3 0 4 7 1 6 2 5
 *  3 6 0 7 4 1 5 2
 *  3 6 2 7 1 4 0 5
 *  4 1 5 0 6 3 7 2
 *  4 1 7 0 3 6 2 5
 *  4 7 3 0 6 1 5 2
 *  5 0 4 1 7 2 6 3
 *  5 2 6 1 7 4 0 3
 *  5 2 6 3 0 7 1 4
 * count = 12
 */
