#include <stdio.h>

/*--- 整数x, yの最大公約数を返却する ---*/
int gcd(int x, int y);

int main() {
  int x, y;

  printf("整数を入力せよ：");
  scanf("%d", &x);

  printf("整数を入力せよ：");
  scanf("%d", &y);

  printf("最大公約数は%dです．\n", gcd(x, y));

  return 0;
}

int gcd(int x, int y) {
  int z = x % y;

  if ( z == 0 )
    return (y);
  else
    return ( gcd(y, z) );
}
