#include <stdio.h>

int count;

int pos[8];
int r_flag[8];
int d_flag[13];
int id_flag[13];

void print();
void set(int i);

int main() {
  set(0);

  printf("count = %d\n", count);

  return 0;
}

void print() {
  for (int i = 0; i < 8; i++) {
    printf("%2d", pos[i]);
  }
  putchar('\n');
}

void set(int n) {
  for (int i = 0; i < 8; i++) {
    if (!r_flag[i] && !d_flag[i+n] && !id_flag[i-n+7]) {
      pos[n] = i;

      if (n == 7) {
        print();
        count++;
      } else {
        r_flag[i] = 1;
        d_flag[i+n] = 1;
        id_flag[i-n+7] = 1;
        set(n + 1);
        r_flag[i] = 0;
        d_flag[i+n] = 0;
        id_flag[i-n+7] = 0;
      }
    }
  }
}
