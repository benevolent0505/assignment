#include <stdio.h>

/**
 * no: 移動すべき円盤の枚数
 * x : 開始軸
 * y : 目的軸
 */
void move(int no, int x, int y);

int main() {
  int n;

  printf("円盤の枚数: ");
  scanf("%d", &n);

  move(n, 1, 3);

  return 0;
}

/* 底の円盤を除いたn-1枚の円盤を，開始軸から中間軸へ移動 */
/* 底の円盤を開始軸から目的軸へ移動 */
/* 底の円盤を除いたn-1枚の円盤を，中間軸から目的軸へ移動 */
void move(int no, int x, int y) {
  if (no > 0) {
    move(no-1, x, 6-x-y);
    printf("[%d]を%d軸から%d軸へ移動\n", no, x, y);
    move(no-1, 6-x-y, y);
  }
}
