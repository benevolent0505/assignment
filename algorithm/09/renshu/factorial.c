#include <stdio.h>

int factorial(int n);
int factorial2(int n);

int main () {
  int x;

  printf("整数を入力せよ：");
  scanf("%d", &x);

  printf("%dの階乗は%dです．\n", x, factorial(x));

  return 0;
}

/*--- 整数値nの階乗を返却 ---*/
int factorial(int n) {
  if(n > 0)
    return n * factorial(n - 1);
  else
    return 1;
}

int factorial2(int n) {
  int ans = 1;

  while(n-->0){             /* n-1, n-2, ... , 1 */
    ans = ans * (n+1);      /* n-- でnが1小さくなってしまっているので+1する  */
  }

  return (ans);
}
