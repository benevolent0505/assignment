/**
 * 課題7-1
 * 提出日 2015/06/03
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: まだポインタがいまいちわかってない気がする。
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char name[10]; /* 名前 */
  int height;    /* 身長 */
  int weight;    /* 体重 */
} Person;

const Person *bin_search(const Person *key, const Person *base, size_t nmemb);

int main() {
  Person x[] = {{"Sugiyama", 165, 50},
                {"Shibata",  170, 52},
                {"Nangoh",   172, 63},
                {"Tsuji",    172, 80},
                {"Takaoka",  180, 70},
  };

  int nx = sizeof(x) / sizeof(x[0]);
  int retry = 0;

  puts("身長による探索を行います。");

  do {
    Person temp;

    printf("身長: ");
    scanf("%d", &temp.height);

    const Person *p = bin_search(&temp, x, nx);

    if (p == NULL) {
      puts("探索に失敗しました。");
    } else {
      printf("x[%d] : %s %dcm %dkg\n",
             (int)(p - x), p->name, p->height, p->weight);
    }

    printf("もう一度探索しますか？ (1)はい/(0)いいえ：");
    scanf("%d", &retry);
  } while (retry == 1);

  return 0;
}

/* keyはキー値へのポインタ、baseは比較対象へのポインタ、nmembは探索対象となる配列要素数 */
const Person *bin_search(const Person *key, const Person *base, size_t nmemb) {
  int pl = 0;
  int pr = nmemb - 1;
  int pc;

  do {
    pc = (pl + pr) / 2;
    if (base[pc].height == key -> height) {
      return  &base[pc];
    } else if (base[pc].height < key -> height) {
      pl = pc + 1;
    } else {
      pr = pc - 1;
    }
  } while (pl <= pr);

  return NULL;
}
/**
 * 身長による探索を行います。
 * 身長: 190
 * 探索に失敗しました。
 * もう一度探索しますか？ (1)はい/(0)いいえ：1
 * 身長: 180
 * x[4] : Takaoka 180cm 70kg
 * もう一度探索しますか？ (1)はい/(0)いいえ：0
 */