/**
 * 課題7-1
 * 提出日 2015/06/03
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: 上限下限のエラーチェックを忘れるところだった.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char name[10]; /* 名前 */
  int height;    /* 身長 */
  int weight;    /* 体重 */
} Person;

const Person *bin_search(int max, int mix, const Person *base, size_t nmemb);

int main() {
  Person x[] = {{"Sugiyama", 165, 50},
                {"Shibata",  170, 52},
                {"Nangoh",   172, 63},
                {"Tsuji",    172, 80},
                {"Takaoka",  180, 70},
  };

  int nx = sizeof(x) / sizeof(x[0]);
  int retry = 0;


  puts("身長による探索を行います。");

  int max, min;

  do {
    printf("身長下限: ");
    scanf("%d", &min);

    do {
      printf("身長上限: ");
      scanf("%d", &max);
    } while (min > max);

    const Person *p = bin_search(max, min, x, nx);

    if (p == NULL) {
      puts("探索に失敗しました。");
    } else {
      printf("x[%d] : %s %dcm %dkg\n",
             (int)(p - x), p->name, p->height, p->weight);
    }

    printf("もう一度探索しますか？ (1)はい/(0)いいえ：");
    scanf("%d", &retry);
  } while (retry == 1);

  return 0;
}

/* keyはキー値へのポインタ、baseは比較対象へのポインタ、nmembは探索対象となる配列要素数 */
const Person *bin_search(int max, int min, const Person *base, size_t nmemb) {
  int pl = 0;
  int pr = nmemb - 1;
  int pc;

  do {
    pc = (pl + pr) / 2;
    if (base[pc].height <= max && base[pc].height >= min) {
      return  &base[pc];
    } else if (base[pc].height < max) {
      pl = pc + 1;
    } else {
      pr = pc - 1;
    }
  } while (pl <= pr);

  return NULL;
}
/**
 * 身長による探索を行います。
 * 身長下限: 150
 * 身長上限: 165
 * x[0] : Sugiyama 165cm 50kg
 * もう一度探索しますか？ (1)はい/(0)いいえ：1
 * 身長下限: 190
 * 身長上限: 200
 * 探索に失敗しました。
 * もう一度探索しますか？ (1)はい/(0)いいえ：0
 */
