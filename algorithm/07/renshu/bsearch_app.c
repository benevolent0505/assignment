#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char name[10]; /* 名前 */
  int height;    /* 身長 */
  int weight;    /* 体重 */
} Person;

/* Person型の比較関数（名前昇順） */
int npcmp(const Person *x, const Person *y);

int main() {
  Person x[] = {{"Nangoh",   172, 63},  /* 配列の要素は名前の昇順で */
                {"Shibata",  170, 52},  /* 並んでいなければならない */
                {"Sugiyama", 165, 50},
                {"Takaoka",  180, 70},
                {"Tsuji",    172, 80},
  };

  int nx = sizeof(x) / sizeof(x[0]);   /* 配列xの要素数 */
  int retry;

  puts("名前による探索を行います。");

  do {
    Person temp, *p;

    printf("名前：");
    scanf("%s", temp.name);

    p = bsearch(&temp, x, nx, sizeof(Person),
                (int (*)(const void *, const void *))npcmp);

    if(p == NULL)
      puts("探索に失敗しました。");
    else
      printf("x[%d] : %s %dcm %dkg\n",
             (int)(p - x), p->name, p->height, p->weight);

    printf("もう一度探索しますか？ (1)はい/(0)いいえ：");
    scanf("%d", &retry);

  } while (retry == 1);

  return 0;
}

/* Person型の比較関数（名前昇順） */
int npcmp(const Person *x, const Person *y) {
  return(strcmp(x->name, y->name));
}
