#include <stdio.h>

int sum(int x1, int x2);

int mul(int x1, int x2);

void kuku(int (*calc)(int,int));

int main(void) {
  puts("九九の足し算表");
  kuku(sum);

  puts("九九のかけ算表");
  kuku(mul);

  return (0);
}

/*--- x1とx2の和を求める ---*/
int sum(int x1, int x2) {
  return (x1 + x2);
}

/*--- x1とx2の積を求める ---*/
int mul(int x1, int x2) {
  return (x1 * x2);
}

/*--- 九九の表を出力する ---*/
void kuku(int (*calc)(int,int)) {
  int i, j;

  for(i=1; i<=9; i++){
    for(j=1; j<=9; j++)
      printf("%3d", (*calc)(i, j));
    putchar('\n');
  }

}
