#include <stdio.h>

int max(int x, int y, int z);
int min(int x, int y, int z);
int mean(int x, int y, int z);

void maxmin(int (*func)(int,int,int), int *x);

int main() {
  int x[3];
  int nx = sizeof(x) / sizeof(x[0]);

  printf("%d個の整数をに入力してください。\n", nx);

  for (int i = 0; i < nx; i++) {
    printf("x[%d]", i);
    scanf("%d", &x[i]);
  }

  maxmin(max, x);
  maxmin(min, x);
  maxmin(mean, x);

  return 0;
}

int max(int x, int y, int z) {
  int max = x;

  if (max <= y) max = y;

  if (max <= z) max = z;

  return max;
}

int min(int x, int y, int z) {
  int min = x;

  if (min >= y) min = y;
  if (min >= z) min = z;

  return min;
}

int mean(int x, int y, int z) {
   return (x + y + z) / 3;
}

void maxmin(int (*func)(int,int,int), int *x) {
  int val = (*func)(x[0], x[1], x[2]);

  if (func == max) {
    printf("最大値: %d\n", val);
  } else if (func == min) {
    printf("最小値: %d\n", val);
  } else if (func == mean) {
    printf("平均値: %d\n", val);
  } else {
    printf("未知のfunction\n");
  }
}
