#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char name[10]; /* 名前 */
  int height;    /* 身長 */
  int weight;    /* 体重 */
} Person;

int htcmp(const Person *x, const Person *y);

int main() {
  Person x[] = {{"Sugiyama", 165, 50},
                {"Shibata",  170, 52},
                {"Nangoh",   172, 63},
                {"Tsuji",    172, 80},
                {"Takaoka",  180, 70},
  };

  int nx = sizeof(x) / sizeof(x[0]);
  int retry = 0;

  puts("身長による探索を行います。");

  do {
    Person temp, *p;

    printf("身長: ");
    scanf("%d", &temp.height);

    p = bsearch(&temp, x, nx, sizeof(Person),
                (int (*)(const void *, const void *))htcmp);

    if (p == NULL) {
      puts("探索に失敗しました。");
    } else {
      printf("x[%d] : %s %dcm %dkg\n",
             (int)(p - x), p->name, p->height, p->weight);
    }

    printf("もう一度探索しますか？ (1)はい/(0)いいえ：");
    scanf("%d", &retry);
  } while (retry == 1);

  return 0;
}

int htcmp(const Person *x, const Person *y) {
  if (x -> height > y -> height) {
    return 1;
  } else if (x -> height < y -> height) {
    return -1;
  }

  return 0;
}
