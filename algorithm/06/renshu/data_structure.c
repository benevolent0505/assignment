#include <stdio.h>

typedef struct __Data {
  int a[5];
  int b[10];
} Data;

/*--- 要素数nの配列aからkeyと一致する要素を線形探索（番兵法）---*/
int search(int a[], int n, int key);

int main() {
  Data data;
  int n_a = sizeof(data.a) / sizeof(data.a[0]);
  int n_b = sizeof(data.b) / sizeof(data.b[0]);

  int key;

  printf("配列a用データの入力：\n");
  printf("%d個の整数を入力してください。\n", n_a - 1);
  for (int i = 0; i < n_a - 1; i++) {
    printf("data.a[%d] : ", i);
    scanf("%d", &data.a[i]);
  }

  /* ここからbの配列 */

  printf("配列b用データの入力：\n");
  printf("%d個の整数を入力してください。\n", n_b - 1);
  for (int i = 0; i < n_b - 1; i++) {
    printf("data.b[%d] : ", i);
    scanf("%d", &data.b[i]);
  }

  printf("探す値 : ");
  scanf("%d", &key);

  int ida = search(data.a, n_a-1, key);
  int idb = search(data.b, n_b-1, key);

  if(ida == -1)
    puts("探索に失敗しました。");
  else
    printf("%dは%d番目にあります。\n", key, ida + 1);

  if(idb == -1)
    puts("探索に失敗しました。");
  else
    printf("%dは%d番目にあります。\n", key, idb + 1);

  return 0;
}

int search(int x[], int n, int key) {
  int i = 0;

  x[n] = key;  /* 番兵を追加 */

  // whileの方が条件分岐の計算量を落とせる
  while (1) {
    if (x[i] == key) break;

    i++;
  }

  return i == n ? -1 : i;
}
