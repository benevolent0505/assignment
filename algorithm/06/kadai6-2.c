/**
 * 課題6-2
 * 提出日 2015/06/03
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: 後置と前置の違いを意識したら、プログラムが一行減らせたのでよかった。
 */

#include <stdio.h>

int searchidx(int a[], int n, int key, int idx[]);

int main() {
  int a[10];
  int na = sizeof(a) / sizeof(a[0]);
  int key;
  int idx[na];

  printf("%d個の整数を入力してください。\n", na);
  for (int i = 0; i < na; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }

  printf("探す値 : ");
  scanf("%d", &key);

  int count = searchidx(a, na, key, idx);

  if (count == 0) {
    puts("探索に失敗しました。");
  } else {
    for (int i = 0; i < count; i++) {
      printf("%dは%d番目にあります。\n", key, idx[i] + 1);
    }
    puts("-------------------------");
    printf("%dは合計%d個みつかりました\n", key, count);
  }

  return 0;
}

int searchidx(int a[], int n, int key, int idx[]) {
  int count = 0;

  // 後置は後足し
  for (int i = 0; i < n; i++) {
    if (a[i] == key) {
      idx[count++] = i;
    }
  }

  return count;
}

/**
 * 10個の整数を入力してください。
 * a[0] : 1
 * a[1] : 2
 * a[2] : 3
 * a[3] : 1
 * a[4] : 2
 * a[5] : 3
 * a[6] : 2
 * a[7] : 3
 * a[8] : 1
 * a[9] : 3
 * 探す値 : 2
 * 2は2番目にあります。
 * 2は5番目にあります。
 * 2は7番目にあります。
 * -------------------------
 * 2は合計3個みつかりました
 */
