/**
 * 課題6-1
 * 提出日 2015/06/03
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: 変数名に悩む.
 */

#include <stdio.h>

int searchRange(int a[], int n, int key1, int key2);

int main() {
  int a[7];
  int na = sizeof(a) / sizeof(a[0]);
  int key1, key2;

  printf("配列a用データの入力：\n");
  printf("%d個の整数を入力してください。\n", na - 1);
  for (int i = 0; i < na - 1; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }

  printf("探す値の範囲(下限): ");
  scanf("%d", &key1);
  printf("探す値の範囲(上限): ");
  scanf("%d", &key2);

  int result = searchRange(a, na - 1, key1, key2);

  if (result == -1) {
    puts("探索に失敗しました。");
  } else {
    printf("%d以上・%d以下の値は%d番目にあります。\n", key1, key2, result + 1);
  }

  return 0;
}

int searchRange(int a[], int n, int key1, int key2) {
  int i = 0;

  a[n] = key1;

  while (1) {
    if (a[i] >= key1 && a[i] <= key2) break;

    i++;
  }

  return i == n ? -1 : i;
}
/**
 * 配列a用データの入力：
 * 6個の整数を入力してください。
 * a[0] : 1
 * a[1] : 3
 * a[2] : 5
 * a[3] : 7
 * a[4] : 9
 * a[5] : 1
 * 探す値の範囲(下限): 4
 * 探す値の範囲(上限): 11
 * 4以上・11以下の値は3番目にあります。
 */
