/**
 * 課題14-1
 * 提出日 2015/07/28
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: distanceのいい感じの省略が思いつかない
 */

#include <stdio.h>

const char A = 'A';

int main() {
  char str[2];
  int distance;

  str[1] = '\0';

  printf("Input a uppercase character: ");
  scanf("%c", str);

  distance = str[0] - A;

  printf("Distance of 'A' is %d\n", distance);

  return 0;
}

/**
 * Input a uppercase character: D
 * Distance of 'A' is 3
 */
