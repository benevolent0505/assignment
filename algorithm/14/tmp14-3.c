#include <stdio.h>
#include <string.h>

int main(void) {
  char str1[100];
  char str2[100];
  int comp;

  printf("文字列1: \n");
  scanf("%s", str1);

  printf("文字列2: \n");
  scanf("%s", str2);

  comp = strncmp(str1, str2, 4);  //文字列を比較する
  printf("comp:%d \n", comp);

  if (comp == 0) {
    printf("文字列%sと%sは等しい \n", str1, str2);
  } else {
    if (comp > 0) {
      printf("文字列%sが大きい  \n", str1);
    } else {
      printf("文字列%sが大きい  \n", str2);
    }
  }

  return 0;
}
