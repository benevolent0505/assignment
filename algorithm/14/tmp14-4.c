#include <stdio.h>
#include <string.h>

int main() {
  char str1[] = "ABCDEF";
  char str2[] = "123";
  char *p = "abcd";

  strcpy(str1, str2);     /* 文字型配列に文字型配列をコピー */
  printf("%s\n", str1);

  strcpy(str1, p);        /* 文字型配列にポインタの指す文字列リテラルをコピー */
  printf("%s\n", str1);

  strcpy(str1, "xyz");    /* 文字型配列に文字列リテラルをコピー */
  printf("%s\n", str1);

  return 0;
}
