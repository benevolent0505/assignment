#include <stdio.h>

int main() {
  char c1 = 'A';
  char c2 = 'Z';
  int tmp;

  tmp = c2 - c1;
  printf("Z-A:%d", tmp);
  printf("\n");

  return 0;
}
