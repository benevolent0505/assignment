#include <stdio.h>
#include <string.h>

char *str_chr(char *str, int c);

int main() {
  char str[100];
  char search_str[2];
  int ch;
  char *i;

  printf("文字列: \n");
  scanf("%s", str);

  printf("探索文字: \n");
  scanf("%s", search_str);

  ch = search_str[0];
  i = str_chr(str, ch);

  if (*i != '\0')
    printf("文字%cは%sの%d番目に存在 \n", ch, str, (int)(i - str + 1));
  else
    printf("文字%cは%sに存在しない \n", ch, str);

  return 0;
}

char *str_chr(char *str, int c) {
  char *tmp = str + (int)strlen(str);
  c = (char)c;

  while (*tmp != c && *tmp != 0) {
    printf("%c, %d\n", *tmp, *tmp);
    tmp--;
  }

  return (char *)tmp;
}
