#include <stdio.h>
#include <string.h>

int str_cmp(const char *, char *);

int main(void) {
  char str1[100];
  char str2[100];
  int comp;

  printf("文字列1: \n");
  scanf("%s", str1);

  printf("文字列2: \n");
  scanf("%s", str2);

  comp = str_cmp(str1, str2);  //文字列を比較する
  printf("comp:%d \n", comp);

  if (comp == 0) {
    printf("文字列%sと%sは等しい \n", str1, str2);
  } else {
    if (comp > 0) {
      printf("文字列%sが大きい  \n", str1);
    } else {
      printf("文字列%sが大きい  \n", str2);
    }
  }

  return 0;
}

int str_cmp(const char *str1, char *str2) {
  while (*str1 == *str2) {
    if (*str1 == '\0') return 0;  //文字列が等しい場合
    str1++;
    str2++;
  }

  return (unsigned char)*str1 -
         (unsigned char)*str2;  //等しくない場合は文字の差を返す
}
