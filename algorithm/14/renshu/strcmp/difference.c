#include <stdio.h>
#include <string.h>

int main() {
  char a[4];
  char b[4];

  a[0] = 'a';
  a[1] = 'b';
  a[2] = 'c';
  a[3] = '\0';

  b[0] = 'Z';
  b[1] = 'Z';
  b[2] = 'Z';
  b[3] = '\0';

  if (strcmp(a, b) == 0) {
    printf("same\n");
  } else {
    printf("different\n");
  }

  return 0;
}
