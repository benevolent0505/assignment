#include <stdio.h>

int str_len(const char *);

int main() {
  char str[100];

  printf("String: \n");
  scanf("%s", str);

  printf("Length of the string: %d \n", str_len(str));

  return 0;
}

int str_len(const char *st) {
  int leng = 0;

  // 終わりはNULL文字だからfalse
  while (st[leng]) leng++;

  return leng;
}
