#include <stdio.h>
#include <string.h>

int main() {
  char str[100];
  char search_str[2];
  int ch;
  char *i;

  printf("文字列: \n");
  scanf("%s", str);

  printf("探索文字: \n");
  scanf("%s", search_str);

  ch = search_str[0];
  i = strchr(str, search_str[0]);

  if (*i != '\0')
    printf("文字%cは%sの%d番目に存在 \n", ch, str, (int)(i - str + 1));
  else
    printf("文字%cは%sに存在しない \n", ch, str);

  return 0;
}
