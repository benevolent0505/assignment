// atoiはASCI to Integer
#include <stdio.h>
#include <stdlib.h>

int main() {
  char str[] = "145";
  char str1[] = "-145";

  int num = atoi(str);
  int num1 = atoi(str1);

  printf("str: %d \n", num);
  printf("str1: %d \n", num1);

  return 0;
}
