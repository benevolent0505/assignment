#include <stdio.h>

void swap_pt(char **, char **);

int main(void) {
  char *pt1 = "CHOFU-L";
  char *pt2 = "CHOFU-R";

  printf("pt1: %s \n", pt1);
  printf("pt2: %s \n", pt2);

  swap_pt(&pt1, &pt2);

  printf("After swap \n");
  printf("pt1: %s \n", pt1);
  printf("pt2: %s \n", pt2);

  return 0;
}

void swap_pt(char **x, char **y) {
  char *tmp = *x;
  *x = *y;
  *y = tmp;
}
