/**
 * 課題14-2
 * 提出日 2015/07/28
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想:
 */

#include <stdio.h>

void swapStr(char *a, char *b);
void swap_pt(char **x, char **y);

int main() {
  char *pt1 = "CHOFU-L";
  char *pt2 = "CHOFU-R";

  printf("pt1: %s \n", pt1);
  printf("pt2: %s \n", pt2);

  swapStr(pt1, pt2);

  printf("After swap \n");
  printf("pt1: %s \n", pt1);
  printf("pt2: %s \n", pt2);

  return 0;
}

void swapStr(char *a, char *b) {
  /* int cnt = 0; */

  /* while (a[cnt] || b[cnt]) { */
  /*   char tmp = a[cnt]; */

  /*   a[cnt] = b[cnt]; */
  /*   b[cnt] = tmp; */

  /*   cnt++; */
  /* } */

  while (*a || *b) {
    printf("%c, %c\n", *a, *b);
    swap_pt(&a, &b);
    printf("%c, %c\n", *a, *b);
    a++;
    b++;
  }
}

void swap_pt(char **x, char **y) {
  char *tmp = *x;
  *x = *y;
  *y = tmp;
}
