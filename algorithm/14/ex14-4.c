/**
 * 課題14-4
 * 提出日 2015/07/28
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想:バッファーオーバーランに気をつけないといけないのに
 */

#include <stdio.h>
#include <string.h>

/**
 * dest[]: コピー先配列
 * src[]: コピー元配列
 */
char *str_cpy(char *dest, const char *src);

int main() {
  char str1[] = "ABCDEF";
  char str2[] = "123";
  char *p = "abcd";

  str_cpy(str1, str2);
  printf("%s\n", str1);

  str_cpy(str1, p);
  printf("%s\n", str1);

  str_cpy(str1, "xyz");
  printf("%s\n", str1);

  return 0;
}

// risk of buffer overrun
char *str_cpy(char *dest, const char *src) {
  int cnt = 0;

  while (dest[cnt]) {
    if (src[cnt]) {
      dest[cnt] = src[cnt];
    } else {
      dest[cnt] = '\0';
    }

    cnt++;
  }

  return dest;
}

/**
 * 123
 * abc
 * xyz
 */
