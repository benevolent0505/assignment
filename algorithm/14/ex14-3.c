/**
 * 課題14-3
 * 提出日 2015/07/28
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: サンプルとの変更が少なくてよかった
 */

#include <stdio.h>
#include <string.h>

int strn_cmp(const char *, char *, size_t size);

int main(void) {
  char str1[100];
  char str2[100];
  int comp;
  int size;

  printf("何文字目まで比較しますか: ");
  scanf("%d", &size);
  printf("文字列1: \n");
  scanf("%s", str1);

  printf("文字列2: \n");
  scanf("%s", str2);

  comp = strn_cmp(str1, str2, size);  //文字列を比較する
  printf("comp:%d \n", comp);

  if (comp == 0) {
    printf("文字列%sと%sは等しい \n", str1, str2);
  } else {
    if (comp > 0) {
      printf("文字列%sが大きい  \n", str1);
    } else {
      printf("文字列%sが大きい  \n", str2);
    }
  }

  return 0;
}

int strn_cmp(const char *str1, char *str2, size_t size) {
  int count = 1;

  while (*str1 == *str2) {
    if (*str1 == '\0') return 0;

    str1++;
    str2++;
    count++;

    if (count == (int)size) break;
  }

  return (unsigned char)*str1 - (unsigned char)*str2;
}

/**
 * 何文字目まで比較しますか:  4
 * 文字列1:
 * Java
 * 文字列2:
 * JavaScript
 * comp:0
 * 文字列JavaとJavaScriptは等しい
 */
