#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 50

void initArray(int array[], int size);
void dump(int array[], int size);
void shellSort(int array[], int size);

int main() {
  int array[N];

  initArray(array, N);
  dump(array, N);

  // shellSort()はdumpもする
  shellSort(array, N);

  return 0;
}

void initArray(int array[], int size) {
  srand((unsigned int)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = (int)(rand() * 100.0 / (1.0 + RAND_MAX));
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    printf("array[%d] = %d\n", i, array[i]);
  }
}

void shellSort(int array[], int size) {
  int h = 1;
  int i, j;
  int comp = 0, exchange = 0;

  // 間隔hの決定
  for (int tmp = 1; tmp < size / 9; tmp = tmp * 3 + 1) {
    h = tmp;
  }

  while (h > 0) {
    for (i = h; i < size; i++) {
      int tmp = array[i];
      for (j = i - h; j >= 0 && array[j] > tmp; j -= h) {
        array[j + h] = array[j];
        comp++;
        exchange++;
      }
      array[j + h] = tmp;
      exchange++;
    }
    h /= 3;
  }

  dump(array, N);
  printf("Compare:%d   Exchange:%d\n", comp, exchange);
}
