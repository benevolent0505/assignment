#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char *name;
  int english;
  int math;
  int physics;
} Grade;

// utility
void swap(Grade *a, Grade *b);

void dumpGrade(Grade *grade[], int size);
int sumGrade(Grade *grade);

// 破壊的
void sortGrade(Grade *grade[], int size);

int main() {
  Grade members[8] = {
    {"Itoh", 20, 10, 50},
    {"Ota", 40, 20, 27},
    {"Akiyama", 12, 90, 22},
    {"Shiota", 29, 43, 9},
    {"Murase", 19, 12, 32},
    {"Kaneko", 3, 89, 43},
    {"Otsuka", 2, 12, 42},
    {"Saito", 28, 78, 74}
  };
  int size = sizeof(members) / sizeof(members[0]);
  Grade *p[size];

  // convert pointer
  for (int i = 0; i < size; i++) {
    p[i] = &members[i];
  }

  puts("before sort");
  dumpGrade(p, size);

  sortGrade(p, size);

  puts("after sort");
  dumpGrade(p, size);
  return 0;
}

void dumpGrade(Grade *grade[], int size) {
  for (int i = 0; i < size; i++) {
    printf("%d: %s: English = %d, Math = %d, Physics = %d\n", i, grade[i]->name,
           grade[i]->english, grade[i]->math, grade[i]->physics);
  }
}

int sumGrade(Grade *grade) {
  return grade->english + grade->math + grade->physics;
}

// shaker sort
void sortGrade(Grade *grade[], int size) {
  int left, right, shift;

  left = 0;
  right = size - 1;
  shift = right;

  while (left < right) {
    for (int i = right; i > left; i--) {
      if (sumGrade(grade[i]) > sumGrade(grade[i - 1])) {
        swap(grade[i], grade[i - 1]);
        shift = i;
      }
    }

    left = shift;

    for (int i = left; i < right; i++) {
      if (sumGrade(grade[i]) < sumGrade(grade[i + 1])) {
        swap(grade[i], grade[i + 1]);
        shift = i;
      }
    }

    right = shift;
  }
}

void swap(Grade *a, Grade *b) {
  Grade tmp = *a;
  *a = *b;
  *b = tmp;
}
