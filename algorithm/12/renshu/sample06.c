#include <stdio.h>

#define N 100

void bidirect_bubble_sort(int *, int);

int main(void) {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int i, k;

  n = 9;

  printf("input %d data: \n", n);  //データ入力
  for (i = 0; i < n; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }
  for (k = 0; k < n; k++)  //データ配列の表示
    printf("a[%d]=%d ", k, a[k]);
  printf(" \n");

  bidirect_bubble_sort(a, n);  //双方向バブルソート

  return 1;
}

void bidirect_bubble_sort(int a[], int n) {
  int i, k, tmp;
  int left, right, shift;

  left = 0;
  right = n - 1;
  shift = right;

  while (left < right) {
    printf("Left=%d Right=%d <- \n", left, right);
    for (i = right; i > left; i--) {  //最小値の要素を先頭方向へ
      if (a[i] < a[i - 1]) {          //要素の比較
        tmp = a[i - 1];               //要素の交換
        a[i - 1] = a[i];
        a[i] = tmp;
        shift = i;  //交換があった位置
      }
    }
    left = shift;            //最後の交換があった位置
    for (k = 0; k < n; k++)  //データ配列の表示
      printf("a[%d]=%d ", k, a[k]);
    printf(" \n");
    printf("Left=%d Right=%d -> \n", left, right);
    for (i = left; i < right; i++) {  //最大値の要素を末尾方向へ
      if (a[i] > a[i + 1]) {          //要素の比較
        tmp = a[i + 1];               //要素の交換
        a[i + 1] = a[i];
        a[i] = tmp;
        shift = i;  //交換があった位置
      }
    }
    right = shift;  //最後の交換があった位置
    for (k = 0; k < n; k++) printf("a[%d]=%d ", k, a[k]);
    printf(" \n");
    printf("Right=%d \n", right);
  }
}
