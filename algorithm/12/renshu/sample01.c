// 基本交換法によって昇順に整列

#include <stdio.h>
#define N 100

void bubble_sort(int *, int);

int main(void) {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int i, k;

  n = 8;

  printf("input %d data: \n", n);  //データ入力
  for (i = 0; i < n; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }
  for (k = 0; k < n; k++)  //データ配列の表示
    printf("a[%d] = %d ", k, a[k]);
  printf(" \n");
  printf("Sorted data in ascending order: \n");

  bubble_sort(a, n);  //バブルソート

  return 0;
}

void bubble_sort(int a[], int n) {
  int i, j, k;
  int comp = 0, exchange = 0;
  int tmp;

  for (i = 0; i < n - 1; i++) {  //パス1からn-1まで繰り返す
    for (j = n - 1; j > i; j--) {
      if (a[j - 1] > a[j]) {  //要素の比較
        tmp = a[j - 1];       //要素の交換
        a[j - 1] = a[j];
        a[j] = tmp;
        exchange++;  //交換回数
      }
      comp++;  //比較回数
    }
    for (k = 0; k < n; k++) printf("a[%d] = %d ", k, a[k]);
    printf(" \n");
  }
  printf("Compare:%d   Exchange:%d \n", comp, exchange);
}
