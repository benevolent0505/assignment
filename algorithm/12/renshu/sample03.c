// 基本挿入法によって配列要素を昇順に整列

#include <stdio.h>
#define N 100

void insertion_sort(int *, int);

int main(void) {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int i, k;

  n = 8;

  printf("input %d data: \n", n);  //データ入力
  for (i = 0; i < n; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }

  for (k = 0; k < n; k++)  //データ配列の表示
    printf("a[%d] = %d ", k, a[k]);
  printf("\n");
  printf("Sorted data in ascending order:\n");

  insertion_sort(a, n);  //基本挿入法

  return 1;
}

void insertion_sort(int a[], int n) {
  int i, j, k;
  int comp = 0, exchange = 0;
  int tmp;

  // 先頭要素をソート済みとみなすので、i = 1から始める
  for (i = 1; i < n; i++) {  //未ソート部分が無くなるまで
    tmp = a[i];  //挿入要素（未ソート部分の先頭要素）
    for (j = i - 1; j >= 0 && a[j] > tmp; j--) {  //ソート済みの列に挿入
      a[j + 1] = a[j];
      comp++;
      exchange++;  //選択回数・交換回数
    }
    a[j + 1] = tmp;  //挿入要素を適切な場所に挿入
    exchange++;      //交換回数
    for (k = 0; k < n; k++) printf("a[%d] = %d", k, a[k]);
    printf("\n");
  }
  printf("Compare:%d   Exchange:%d \n", comp, exchange);
}
