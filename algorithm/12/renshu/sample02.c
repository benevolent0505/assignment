// 基本選択法によって配列要素を昇順に整列

#include <stdio.h>
#define N 100

void selection_sort(int *, int);

int main(void) {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int i, k;

  n = 8;

  printf("input %d data: \n", n);  //データ入力
  for (i = 0; i < n; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }

  for (k = 0; k < n; k++)  //データ配列の表示
    printf("a[%d] = %d ", k, a[k]);
  printf(" \n");
  printf("Sorted data in ascending order: \n");

  selection_sort(a, n);  //基本選択法

  return 1;
}

void selection_sort(int a[], int n) {
  int i, j, k;
  int comp = 0, exchange = 0;
  int min, tmp;

  for (i = 0; i < n - 1; i++) {  //未ソート部分が無くなるまで
    min = i;
    for (j = i + 1; j < n; j++) {  //未ソート部分の最小値を選択
      if (a[j] < a[min]) min = j;
      comp++;  //比較回数
    }
    tmp = a[i];  //最小値と未ソート部分の先頭要素を交換
    a[i] = a[min];
    a[min] = tmp;
    exchange++;  //交換回数
    for (k = 0; k < n; k++) printf("a[%d] = %d ", k, a[k]);
    printf("\n");
  }
  printf("Compare:%d   Exchange:%d \n", comp, exchange);
}
