#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 100

typedef struct {
  char *name;
  int age;
} Members;

void selection_sort(Members *[], int);

int main(void) {
  Members a[] = {{"Itoh", 24},   {"Ota", 27},   {"Akiyama", 22}, {"Shiota", 29},
                 {"Murase", 19}, {"Kaneko", 3}, {"Otsuka", 2},   {"Saito", 28}};

  int n = sizeof(a) / sizeof(a[0]);  //データ数
  int i;
  Members *p[n];

  for (i = 0; i < n; i++)  //ポインタ配列の準備
    p[i] = &a[i];

  printf("database: %d\n", n);  //初期データ集合の出力
  for (i = 0; i < n; i++)
    printf("%d: %s  Age = %d\n", i, p[i]->name, p[i]->age);
  printf("\n");

  selection_sort(p, n);  //基本選択法でのソート

  return 1;
}

void selection_sort(Members *p[], int n) {
  int i, min_i, j, k;
  char *min;
  Members *tmp;

  for (i = 0; i < n - 1; i++) {
    min = p[i]->name;
    min_i = i;
    for (j = i + 1; j < n; j++) {
      if (strcmp(p[j]->name, min) < 0) {  //未ソート部分の最小値となる名前を選択
        min = p[j]->name;
        min_i = j;
      }
    }
    tmp = p[i];  //最小値と未ソート部分の先頭要素を交換
    p[i] = p[min_i];
    p[min_i] = tmp;

    for (k = 0; k < n; k++)
      printf("%d: %s  Age = %d\n", k, p[k]->name, p[k]->age);
    printf("\n");
  }
}
