#include <stdio.h>

#define N 100

void bubble_sort(int *, int);

int main(void) {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int i, k;

  n = 7;

  printf("input %d data:\n", n);  //データ入力
  for (i = 0; i < n; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }
  for (k = 0; k < n; k++)  //データ配列の表示
    printf("a[%d] = %d", k, a[k]);
  printf("\n");

  bubble_sort(a, n);  //バブルソート

  return 1;
}

void bubble_sort(int a[], int n) {
  int j, k, q;
  int comp = 0, exchange = 0;
  int terminal, tmp;

  k = 0;

  while (k < n - 1) {  //パス1からn-1まで繰り返す
    printf("The left terminal = %d\n", k);
    terminal = n - 1;
    for (j = n - 1; j > k; j--) {  //前回最後の交換があった位置まで繰り返す
      if (a[j - 1] > a[j]) {  //要素の比較
        tmp = a[j - 1];
        a[j - 1] = a[j];
        a[j] = tmp;
        terminal = j;  //交換があった位置
        exchange++;    //交換回数
      }
      comp++;  //比較回数
    }
    k = terminal;  //最後の交換があった位置
    for (q = 0; q < n; q++) printf("a[%d] = %d", q, a[q]);
    printf(" \n");
  }
  printf("Compare:%d   Exchange:%d\n", comp, exchange);
}
