/**
 * 課題13-2
 * 提出日 2015/07/21
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: pl, prの初期化部分をもう少しマシにしたい.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void initArray(int array[], int size);
void scanArray(int array[], int size);
void dump(int array[], int size);
void swap(int *a, int *b);

void quickSort(int array[], int left, int right);

int main() {
  int array[10];
  int size = SIZE_OF_ARRAY(array);

  initArray(array, size);
  dump(array, size);

  puts("sort");
  quickSort(array, 0, size - 1);

  dump(array, size);

  return 0;
}

// 配列を0~99までの乱数で初期化
void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 100 + 1;
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}

void quickSort(int array[], int left, int right) {
  int pl, pr;
  int pivot;

  if (left < right) {
    pivot = array[(left + right) / 2];
    pl = left - 1;
    pr = right + 1;

    do {
      pl++;
      pr--;

      while (array[pl] < pivot) pl++;
      while (array[pr] > pivot) pr--;

      if (pl < pr) swap(&array[pl], &array[pr]);
    } while (pl < pr);

    quickSort(array, left, pl - 1);
    quickSort(array, pr + 1, right);
  }
}

/**
 * array[0] = 79, array[1] = 10, array[2] = 68, array[3] = 10, array[4] = 53, array[5] = 92, array[6] = 94, array[7] = 12, array[8] = 89, array[9] = 23
 * sort
 * array[0] = 10, array[1] = 10, array[2] = 12, array[3] = 23, array[4] = 53, array[5] = 68, array[6] = 79, array[7] = 89, array[8] = 92, array[9] = 94
 */
