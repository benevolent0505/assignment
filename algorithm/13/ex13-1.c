/**
 * 課題13-1
 * 提出日 2015/07/21
 * 学籍番号 1410114
 * 氏名 藤田 幹央
 * 感想: XXのコード(再帰の左側の部分)を参考にしました.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// utilities
#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void initArray(int array[], int size);
void dump(int array[], int size);
void swap(int *a, int *b);

void quickSort(int array[], int left, int right);

int main() {
  int array[10];
  int size = SIZE_OF_ARRAY(array);

  initArray(array, size);
  dump(array, size);

  puts("sorted");

  quickSort(array, 0, size - 1);

  dump(array, size);

  return 0;
}

void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 100 + 1;
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}

void quickSort(int array[], int left, int right) {
  int middle;
  int pl, pr;
  int pivot;

  if (left < right) {
    middle = (left + right) / 2;
    swap(&array[left], &array[middle]);
    pivot = array[left];

    pl = left;
    pr = right + 1;  // +1はdo-whileの最初でpr--をするため

    do {
      pl++;
      pr--;

      while (array[pl] < pivot) pl++;
      while (array[pr] > pivot) pr--;

      if (pl < pr) swap(&array[pl], &array[pr]);
    } while (pl < pr);
    // pr以降のものはpivotよりも大きいので
    array[left] = array[pr];
    array[pr] = pivot;

    // prは絶対に止まるので
    quickSort(array, left, pr - 1);
    quickSort(array, pr + 1, right);
  }
}

/**
 * array[0] = 99, array[1] = 76, array[2] = 99, array[3] = 84, array[4] = 51, array[5] = 71, array[6] = 21, array[7] = 98, array[8] = 22, array[9] = 27
 * sorted
 * array[0] = 21, array[1] = 22, array[2] = 27, array[3] = 51, array[4] = 71, array[5] = 76, array[6] = 84, array[7] = 98, array[8] = 99, array[9] = 99
 */

