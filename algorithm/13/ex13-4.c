// 一つの配列を中央から二つ分割し,それぞれをsortした後にmergeするsortを作りなさい.なお,各部分のsortにもmerge
// sortを用いること.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void initArray(int array[], int size);
void dump(int array[], int size);
void swap(int *a, int *b);

void mergeSort(int array[], int left, int right);

int main() {
  int array[10];
  int size = SIZE_OF_ARRAY(array);

  initArray(array, size);
  dump(array, size);

  puts("sort");
  mergeSort(array, 0, size - 1);

  dump(array, size);

  return 0;
}

// 配列を0~99までの乱数で初期化
void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 10 + 1;
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}

void mergeSort(int array[], int left, int right) {
  if (left < right) {
    int center = (left + right) / 2;

    int pl = left;
    int pr = center + 1;

    if (left != center) {
      mergeSort(array, left, center);
      mergeSort(array, center + 1, right);
    }

    // merge
  }
}
