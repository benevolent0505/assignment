// 配列の先頭要素,中央要素,末尾要素の3つのうち中央値を持つ要素を軸としてquick
// sort
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void initArray(int array[], int size);
void dump(int array[], int size);
void swap(int *a, int *b);

void quickSort(int array[], int left, int right);

int main() {
  int array[10];
  int size = SIZE_OF_ARRAY(array);

  initArray(array, size);
  dump(array, size);

  puts("sort");
  quickSort(array, 0, size - 1);

  dump(array, size);

  return 0;
}

// 配列を0~99までの乱数で初期化
void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 100 + 1;
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}

void quickSort(int array[], int left, int right) {
  int middle = (left + right) / 2;
  int pl, pr;
  int pivot;

  if (left < right) {
    // pivot決め
    if (array[left] < array[middle]) {
      if (array[right] < array[left]) {
        pivot = array[left];
      } else if (array[middle] < array[right]) {
        pivot = array[middle];
      } else {
        pivot = array[right];
      }
    } else if (array[left] > array[middle]) {
      if (array[left] < array[right]) {
        pivot = array[left];
      } else if (array[right] < array[middle]) {
        pivot = array[middle];
      } else {
        pivot = array[right];
      }
    } else {
      pivot = array[middle];
    }

    pl = left - 1;
    pr = right + 1;

    do {
      pl++;
      pr--;

      while (array[pl] < pivot) pl++;
      while (array[pr] > pivot) pr--;

      if (pl < pr) swap(&array[pl], &array[pr]);
    } while (pl < pr);

    quickSort(array, left, pl - 1);
    quickSort(array, pr + 1, right);
  }
}
