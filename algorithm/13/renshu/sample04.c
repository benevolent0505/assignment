#include <stdio.h>

#define N 50

int *buff;

void merge(int *, int, int *, int, int *);

int main(void) {
  int a[5] = {3, 5, 7, 8, 10};        //整列済み配列aの設定
  int an = sizeof(a) / sizeof(a[0]);  // aの要素数
  int b[4] = {1, 3, 6, 15};           //整列済み配列bの設定
  int bn = sizeof(b) / sizeof(b[0]);  // bの要素数
  int i;
  int c[an + bn];  //マージ後の配列

  for (i = 0; i < an; i++) {  //配列aの出力
    printf("a[%d]= %d ", i, a[i]);
  }
  printf("\n");
  for (i = 0; i < bn; i++) {  //配列bの出力
    printf("b[%d]= %d ", i, b[i]);
  }

  merge(a, an, b, bn, c);  //マージ

  printf("\n");
  printf("===================\n");

  for (i = 0; i < an + bn; i++)  //配列cの出力
    printf("c[%d]= %d ", i, c[i]);
  printf("\n");

  return 1;
}

void merge(int a[], int an, int b[], int bn, int c[]) {
  int pa = 0;
  int pb = 0;
  int pc = 0;

  while (pa < an && pb < bn)  // aとbのマージ
    c[pc++] =
        (a[pa] <= b[pb])
            ? a[pa++]
            : b[pb++];  // aの要素がbよりも小さいか等しいときにaの要素をCへ出力
  while (pa < an)  // 比較後，配列aに要素が余っている場合はcへ出力
    c[pc++] = a[pa++];
  while (pb < bn)  //比較後，配列bに要素が余っている場合はcへ出力
    c[pc++] = b[pb++];
}
