#include <stdio.h>

#define SIZE 10

int select_pivot(int data[], int begin, int end);
void quickSort(int left, int right, int data[]);
void displayData(int data[]);

int main(int argc, char const *argv[]) {
  int data[SIZE];

  printf("input %d data: \n", SIZE);
  for (int i = 0; i < SIZE; i++) {
    printf("data[%d] : ", i);
    scanf("%d", &data[i]);
  }

  printf("---");
  displayData(data);

  quickSort(0, SIZE - 1, data);

  return 0;
}

void quickSort(int left, int right, int data[]) {
  int pivot, tmp;
  int pl, pr;

  if (right > left) {
    pivot = select_pivot(data, left, right);

    pl = left - 1;
    pr = right + 1;

    while (1) {
      pl++;
      pr--;
      while (data[pl] < pivot) pl++;
      while (data[pr] > pivot) pr--;
      if (pl >= pr) {
        break;
      }
      tmp = data[pl];
      data[pl] = data[pr];
      data[pr] = tmp;
      printf("pl: %d pr: %d \n", pl, pr);
    }

    printf("pivot: %d-- ", pivot);
    displayData(data);

    printf("the sequence of elements less than the pivot: \n");
    for (int i = left; i <= pivot - 1; i++) {
      printf("%d ", data[i]);
    }
    printf("\n");
    printf("pivot: %d \n", pivot);
    printf("the sequence of elements more than pivot: \n");
    for (int i = pivot + 1; i <= right; i++) {
      printf("%d ", data[i]);
    }
    printf("\n");

    quickSort(left, pl - 1, data);
    quickSort(pr + 1, right, data);
  }
}

void displayData(int data[]) {
  for (int i = 0; i < SIZE; i++) {
    printf("%d ", data[i]);
  }
  printf("\n");
}

int select_pivot(int data[], int begin, int end) {
  int center = (begin + end) / 2;

  if (data[begin] <= data[center]) {
    if (data[center] <= data[end]) {
      return data[center];
    } else if (data[end] <= data[begin]) {
      return data[begin];
    }
    return data[end];
  } else {
    if (data[end] <= data[center]) {
      return data[center];
    } else if (data[begin] <= data[end]) {
      return data[begin];
    }
    return data[end];
  }
}
