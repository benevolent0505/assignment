#include <stdio.h>

void partition(int *, int, int);

int main(void) {
  int i, k;
  int a[6];
  int n = sizeof(a) / sizeof(a[0]);

  printf("input %d data: \n", n);  //要素の入力
  for (i = 0; i < n; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }
  for (k = 0; k < n; k++) printf("a[%d]=%d ", k, a[k]);  //要素列の出力
  printf(" \n");
  partition(a, 0, n - 1);

  return 1;
}

void partition(int a[], int left, int right) {
  int i, tmp;
  int pl, pr;
  int pivot;

  if (left < right) {
    pivot = a[left];  //軸の設定
    pl = left;
    pr = right + 1;  //左側ポインタ・右側ポインタの初期化
    do {
      pl++;
      pr--;
      while (a[pl] < pivot) pl++;              //左側ポインタの移動
      while (a[pr] > pivot) pr--;              //右側ポインタの移動
      printf("pl : %d   pr : %d \n", pl, pr);  //ポインタの出力
      if (pl < pr) {                           //値の交換
        tmp = a[pl];
        a[pl] = a[pr];
        a[pr] = tmp;
      }
      printf("pl : %d   pr : %d \n", pl, pr);  //ポインタの出力
    } while (pl < pr);
    a[left] = a[pr];
    a[pr] = pivot;  //軸と要素の交換

    printf("the sequence of elements less than the pivot:  \n");
    for (i = left; i <= pr - 1; i++) printf("%d  ", a[i]);
    printf(" \n");
    printf("pivot : %d \n", pivot);
    printf("the sequence of elements more than pivot:  \n");
    for (i = pr + 1; i <= right; i++) printf("%d  ", a[i]);
    printf(" \n");
  }
}
