#include <stdio.h>
#define N 100

void partition(int *, int, int);

int main(void) {
  int i, k;
  int a[10];
  int n = sizeof(a) / sizeof(a[0]);

  printf("input %d data: \n", n);
  for (i = 0; i < n; i++) {  //要素の入力
    printf("a[%d] : ", i);
    scanf("%d", &a[i]);
  }
  for (k = 0; k < n; k++) printf("a[%d]=%d ", k, a[k]);  //要素列の出力
  printf(" \n");

  partition(a, 0, n - 1);

  for (k = 0; k < n; k++) printf("a[%d]=%d ", k, a[k]);  //要素列の出力

  return 0;
}

void partition(int a[], int left, int right) {
  int i, middle, tmp;
  int pl, pr;
  int pivot;

  if (left < right) {
    middle = (left + right) / 2;  //中央の要素
    tmp = a[middle];
    a[middle] = a[left];
    a[left] = tmp;    //先頭要素と中央要素の交換
    pivot = a[left];  //軸の設定
    pl = left;
    pr = right + 1;  //左側ポインタ・右側ポインタの初期化
    do {
      pl++;
      pr--;
      while (a[pl] < pivot) pl++;              //左側ポインタの移動
      while (a[pr] > pivot) pr--;              //右側ポインタの移動
      printf("pl : %d   pr : %d \n", pl, pr);  //ポインタの出力
      if (pl < pr) {                           //値の交換
        tmp = a[pl];
        a[pl] = a[pr];
        a[pr] = tmp;
      }
      printf("pl : %d   pr : %d \n", pl, pr);  //ポインタの出力
    } while (pl < pr);
    a[left] = a[pr];
    a[pr] = pivot;  //軸と要素の交換

    printf("the sequence of elemtns less than the pivot:  \n");
    for (i = left; i <= pr - 1; i++) printf("%d  ", a[i]);
    printf(" \n");
    printf("pivot : %d \n", pivot);
    printf("the sequence of elemtns more than pivot:  \n");
    for (i = pr + 1; i <= right; i++) printf("%d  ", a[i]);
    printf(" \n");

    partition(a, left, pl - 1);
    partition(a, pr + 1, right);
  }
}
