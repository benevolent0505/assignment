// 課題 13-1

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10

void quickSort(int left, int right, int data[]);
void displayData(int data[]);
void scanArray(int array[], int size);
void initArray(int array[], int size);
void dump(int array[], int size);

int main(int argc, char const *argv[]) {
  int data[SIZE];

  scanArray(data, SIZE);

  printf("---");
  displayData(data);

  quickSort(0, SIZE - 1, data);

  dump(data, SIZE);

  return 0;
}

void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 10 + 1;
  }
}

void scanArray(int array[], int size) {
  printf("input %d data:\n", size);

  for (int i = 0; i < size; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &array[i]);
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void quickSort(int left, int right, int data[]) {
  int pivot, tmp;
  int pl, pr, middle;

  if (right > left) {
    middle = (left + right) / 2;

    tmp = data[middle];
    data[middle] = data[left];
    data[left] = tmp;

    pivot = data[left];

    pl = left;
    pr = right + 1;

    while (1) {
      pl++;
      pr--;
      while (data[pl] < pivot) pl++;
      while (data[pr] > pivot) pr--;
      if (pl >= pr) {
        break;
      }
      tmp = data[pl];
      data[pl] = data[pr];
      data[pr] = tmp;
      printf("pl: %d pr: %d \n", pl, pr);
    }
    data[left] = data[pr];
    data[pr] = pivot;

    printf("pivot: %d-- ", pivot);
    displayData(data);

    printf("the sequence of elements less than the pivot: \n");
    for (int i = left; i <= pivot - 1; i++) {
      printf("%d ", data[i]);
    }
    printf("\n");
    printf("pivot: %d \n", pivot);
    printf("the sequence of elements more than pivot: \n");
    for (int i = pivot + 1; i <= right; i++) {
      printf("%d ", data[i]);
    }
    printf("\n");

    quickSort(left, pr - 1, data);
    quickSort(pr + 1, right, data);
  }
}

void displayData(int data[]) {
  for (int i = 0; i < SIZE; i++) {
    printf("%d ", data[i]);
  }
  printf("\n");
}
