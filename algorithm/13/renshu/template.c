#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void initArray(int array[], int size);
void scanArray(int array[], int size);
void dump(int array[], int size);
void swap(int *a, int *b);

int main() { return 0; }

// 配列を0~99までの乱数で初期化
void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 10 + 1;
  }
  XF
}

void scanArray(int array[], int size) {
  printf("input %d data:\n", size);

  for (int i = 0; i < size; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &array[i]);
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}
