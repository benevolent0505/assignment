#include <stdio.h>
#include <stdlib.h>

#define N 50

void shiftdown(int, int, int *);

int main(void) {
  int a[N];  // the maxmum size of array is fixed at N
  int n;     // array size
  int i, j, m, parent, child, tmp;

  a[0] = 8;  // 2分木
  a[1] = 4;
  a[2] = 6;
  a[3] = 5;
  a[4] = 3;
  a[5] = 9;
  a[6] = 2;
  a[7] = 1;
  a[8] = 7;

  n = 8;

  for (j = 0; j <= n; j++) printf("a[%d]=%d  ", j, a[j]);
  printf("\n");

  for (i = (n - 1) / 2; i >= 0; i--) {  //ヒープの構成
    parent = i;
    child = parent * 2 + 1;
    shiftdown(parent, n, a);
    for (j = 0; j <= n; j++) printf("a[%d]=%d  ", j, a[j]);
    printf("\n");
  }
  m = n;
  printf("===================");
  printf("\n");
  while (m > 0) {  //根の削除とヒープの再構成
    tmp = a[0];    //根と最終要素との交換
    a[0] = a[m];
    a[m] = tmp;
    m--;
    shiftdown(0, m, a);
    for (j = 0; j <= m; j++) printf("a[%d]=%d  ", j, a[j]);
    printf("\n");
  }
  for (j = 0; j <= n; j++) printf("a[%d]= %d  ", j, a[j]);
  printf("\n");

  return 1;
}

void shiftdown(int parent, int n, int heap[]) {
  int child;
  int tmp;

  child = parent * 2 + 1;
  while (child <= n) {
    if (child < n && heap[child] < heap[child + 1]) child++;
    if (heap[parent] < heap[child]) {
      tmp = heap[parent];
      heap[parent] = heap[child];
      heap[child] = tmp;
    }
    parent = child;
    child = parent * 2 + 1;
  }
}
