#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE_OF_ARRAY(array) sizeof(array) / sizeof(array[0])

void initArray(int array[], int size);
void scanArray(int array[], int size);
void dump(int array[], int size);
void swap(int *a, int *b);

void quickSort(int array[], int left, int right);

int main() {
  int array[6];
  int size = SIZE_OF_ARRAY(array);

  initArray(array, size);
  dump(array, size);

  quickSort(array, 0, size - 1);

  // dump(array, size);
  return 0;
}

// 配列を0~99までの乱数で初期化
void initArray(int array[], int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    array[i] = rand() % 10 + 1;
  }
}

void scanArray(int array[], int size) {
  printf("input %d data:\n", size);

  for (int i = 0; i < size; i++) {
    printf("a[%d] : ", i);
    scanf("%d", &array[i]);
  }
}

void dump(int array[], int size) {
  for (int i = 0; i < size; i++) {
    if (i != 0) printf(", ");
    printf("array[%d] = %d", i, array[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int tmp = *a;

  *a = *b;
  *b = tmp;
}

void quickSort(int array[], int left, int right) {
  int pivot;
  int pl, pr;
  int middle = (left + right) / 2;

  if (left < right) {
    pl = left;
    pr = right;
    pivot = array[middle];

    swap(&array[left], &array[middle]);

    while (pl < pr) {
      while (array[pl] < pivot) pl++;
      while (array[pr] > pivot) pr--;

      printf("pl : %d, pr : %d\n", pl, pr);
      if (pl < pr) {
        swap(&array[pl], &array[pr]);
        puts("swap");

        pl++;
        pr--;
      }
      printf("pl : %d, pr : %d\n", pl, pr);
      dump(array, 6);

      quickSort(array, left, pl - 1);
      quickSort(array, pr + 1, right);
    }
  }
}
