#include <stdio.h>
#include <stdlib.h>

struct cell {
  int key;			/* キーひとつだけの構造体 */
  struct cell *left;
  struct cell *right;
};


void dump( struct cell *tree, int c ) {		/* ダンプ出力用関数 */
  if ( tree->left  != NULL ) { dump( tree->left,  c+3 ); }
  fprintf( stdout, "%*s%d\n", c, "", tree->key );
  if ( tree->right != NULL ) { dump( tree->right, c+4 ); }
}


void insert( int key, struct cell **tree ) {	/* キー挿入, tree は ** であることに注意 */
  struct cell *p;

  if ( *tree == NULL ) {
    *tree = p = (struct cell *) malloc( sizeof( struct cell ) );
    p->key = key;
    p->left = p->right = NULL;
  } else {
    if ( key == (*tree)->key ) {
      fprintf( stderr, "key: %d already exists ...\n", key );
    } else if ( key < (*tree)->key ) {
      insert( key, &((*tree)->left) );
    } else {
      insert( key, &((*tree)->right) );
    }
  }
}


struct cell *find_max( struct cell *tree ) {	/* 部分木中の最大のキーを探す */
  if ( tree->right ) {
    return find_max( tree->right );
  } else {
    return tree;
  }
}


void delete( int key, struct cell **tree ) {	/* キー削除, tree は ** であることに注意 */
  struct cell *p;

  if ( *tree == NULL ) {			/* ポインタが無かったら終了 */
    fprintf( stderr, "key: %d not found\n", key );
  } else {
    if ( key == (*tree)->key ) {		/* 削除するキーを発見 */
      if ( (*tree)->left && (*tree)->right ) {	/* 部分木が二つあるとき */
        p = find_max( ((*tree)->left) );
        (*tree)->key = p->key;
        delete( p->key, &((*tree)->left) );
      } else if ( (*tree)->left ) {		/* 部分木は左側だけ */
        p = (*tree)->left;
        free( *tree );
        *tree = p;
      } else {					/* 部分木はあっても右側だけ */
        p = (*tree)->right;
        free( *tree );
        *tree = p;
      }
    } else if ( key < (*tree)->key ) {
      delete( key, &((*tree)->left) );
    } else {
      delete( key, &((*tree)->right) );
    }
  }
}


int main() {
  struct cell *root;

  root = NULL;
  insert( 1000,  &root );
  insert( 100,   &root );
  insert( 10000, &root );
  insert( 10,    &root );
  insert( 200,   &root );
  insert( 500,   &root );
  insert( 2000,  &root );

  dump( root, 0 );

  delete( 200, &root );
  delete( 2000, &root );
  delete( 1000, &root );
  delete( 10000, &root );
  fprintf( stdout, "\n" );

  dump( root, 0 );

  return 0;
}
