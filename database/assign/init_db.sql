CREATE TABLE Users(
user_id integer primary key autoincrement,
username character(30),
email character(30)
);
INSERT INTO "Users" VALUES(1,'Jack', 'jack@example.com');
INSERT INTO "Users" VALUES(2,'Biz', 'biz@example.com');
INSERT INTO "Users" VALUES(3,'Noah', 'noah@foobar.com');
INSERT INTO "Users" VALUES(4,'Evan', 'evan@foobar.com');
INSERT INTO "Users" VALUES(5, 'Mikio', 'mikio@example.com');

CREATE TABLE Tweets(
status_id integer primary key autoincrement,
status character(140),
user_id integer,
foreign key (user_id) references Users(user_id)
);

INSERT INTO "Tweets" VALUES(1,'just setting up my twttr',1);
INSERT INTO "Tweets" VALUES(2,'just setting up my twttr',3);
INSERT INTO "Tweets" VALUES(3,'checking out twttr',4);
INSERT INTO "Tweets" VALUES(4, 'this is first tweet.', 5);
INSERT INTO "Tweets" VALUES(5, 'this is second tweet.', 5);
INSERT INTO "Tweets" VALUES(6, 'this is third tweet.', 5);
